package com.delivery_app.qa.testsuite;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.CashPage;
import com.delivery_app.qa.pages.DocumentsPage;
import com.delivery_app.qa.pages.EmergencyNumberPage;
import com.delivery_app.qa.pages.MainScreenPage;
import com.delivery_app.qa.pages.PaymentPage;
import com.delivery_app.qa.pages.PenaltyPage;
import com.delivery_app.qa.pages.PendingPage;
import com.delivery_app.qa.pages.ScanDemandPage;
import com.delivery_app.qa.pages.StarPerformerPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Menu Screen and Home Page")
@Epic("Regression Suite")
public class RegressionSuites extends TestBase {
	
	PendingPage pendingPage;
	DocumentsPage documentsPage;
	EmergencyNumberPage emergencyNumber;
	PaymentPage paymentPage;
	ScanDemandPage scanPage;
	StarPerformerPage starPerformer;
	MainScreenPage mainscreen;
	CashPage cashPage;
	PenaltyPage penaltyPage;

	

	public RegressionSuites() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		pendingPage = new PendingPage();
		documentsPage= new DocumentsPage();
		emergencyNumber=new EmergencyNumberPage();
		paymentPage=new PaymentPage();
		scanPage= new ScanDemandPage();
		starPerformer= new StarPerformerPage();
		mainscreen= new MainScreenPage();
		cashPage=new CashPage();
		penaltyPage=new PenaltyPage();
		
	}
	
	
	
	@Test(enabled=false,priority = 0, description = "verify that main page")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: verify main page")
	@Link("CD Partner without FnV version 8.6.apk")
	@Story("Story Name : maian page")
	public void verifyMainScreen()throws Exception {
		
		 try {
			 

		    mainscreen.verifyonebyonelinkMainScreen();			
			WebElement MainScreenPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Pending']"));
			String expected_text = "Pending";
			Assert.assertEquals(MainScreenPage.getText(), expected_text);
				
			} catch (Exception e) {

		      System.out.println("Failed case");	
		      
				WebElement MainScreenPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Pending']"));
				String expected_text = "Pending111";
				Assert.assertEquals(MainScreenPage.getText(), expected_text);
			
			}
		
	}
	

	@Test(priority = 1, description = "verify that Penalty Screen")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon thanafter click on penalty button")
	@Story("Story Name : verify that Penalty Screen")
	public void penalty_Page() throws Exception {
		

		 try {
			   penaltyPage.PenaltyScreen();

				WebElement PenaltyPage = driver.findElement(By.xpath("//android.widget.TextView[@text='This Month']"));			
				String expected_text ="This Month";
				Assert.assertEquals(PenaltyPage.getText(),expected_text );
	
		
			} catch (Exception e) {

				System.out.println("Failed cases");

			}
			
		}	


	@Test(enabled=false,priority = 2, description = "verify that Scan demand page")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: verify Scan demand page")
	@Link("CD Partner without FnV version 8.6.apk")
	@Story("Story Name : Scan demand page")
	public void verifyScanDemandPage() throws Exception {
		
		 try {
			 

			   scanPage.ScanDemand();
			   Thread.sleep(4000);
			   
			    WebElement ScanDemandPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Scan Demand']"));
				String expected_text = "Scan Demand";
				Assert.assertEquals(ScanDemandPage.getText(), expected_text);
				
			} catch (Exception e) {

		      System.out.println("Failed case");
			    WebElement ScanDemandPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Scan Demand']"));
				String expected_text = "Scan Demand111";
				Assert.assertEquals(ScanDemandPage.getText(), expected_text);
			
			}
		
	}
	
	
	@Test(enabled=false,priority = 3, description = "verify that first click on drawer icon thanafter click on payment link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon thanafter click on payment link")
	@Story("Story Name : click on payment link")
	public void verifyPaymentPage() throws Exception {

	 try {
			paymentPage.Payments();

			WebElement PaymentPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Payments']"));
			String expected_text = "Payments";
			Assert.assertEquals(PaymentPage.getText(), expected_text);

			
			
		} catch (Exception e) {
			
          System.out.println("Falied case");

			WebElement PaymentPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Payments']"));
			String expected_text = "Payments11";
			Assert.assertEquals(PaymentPage.getText(), expected_text);
			
		}
		
	}		
	
	
	/*
	 * document page
	 */
	
	@Test(enabled=false,priority = 4, description = "verify that document page")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: verify document page")
	@Link("CD Partner without FnV version 8.6.apk")
	@Story("Story Name : document page")
	public void verifyDocumentPage() throws Exception {
		
			
		 try {
				documentsPage.Documents();

				WebElement DocumentPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Please upload all the required document for uninterrupted app.']"));			
				String expected_text ="Please upload all the required document for uninterrupted app.";
				Assert.assertEquals(DocumentPage.getText(),expected_text );
	
		
			} catch (Exception e) {

				System.out.println("Failed cases");

				WebElement DocumentPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Please upload all the required document for uninterrupted app.']"));			
				String expected_text ="Please upload all the required document for uninterrupted app.111";
				Assert.assertEquals(DocumentPage.getText(),expected_text );
			}
			
		}
		
	
	@Test(enabled=false,priority = 5, description = "To Validate that clicking on star performanc field")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: In Drawer Screen,clicking on star performanc field ")
	@Story("Story Name : clicking on star performanc field ")
	public void starPerformancePage() throws Exception {

		try {
		//click on drawer icon
	   	starPerformer.StarPerformer();
		
	   	WebElement starPerformance = driver.findElement(By.xpath("//android.widget.TextView[@text='Star Performer']"));			
		String expected_text ="Star Performer";
		Assert.assertEquals(starPerformance.getText(),expected_text );

		Thread.sleep(3000);
		
		
		} catch (Exception e) {

			 System.out.println("Failed case");	
			 
				
			   	WebElement starPerformance = driver.findElement(By.xpath("//android.widget.TextView[@text='Star Performer']"));			
				String expected_text ="Star Performer111";
				Assert.assertEquals(starPerformance.getText(),expected_text );
			
			}
	}
	
	

	@Test(enabled=false,priority = 6, description = "verify that Emergency New page")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: verify Emergency New page")
	@Link("CD Partner without FnV version 8.6.apk")
	@Story("Story Name : Emergency New page")

	public void VerifyEmergencyNumberPage() {
		try {
		
			emergencyNumber.EmergencyNumber();
			Thread.sleep(4000);

		   	WebElement EmergencyPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Penalty']"));			
			String expected_text ="Penalty";
			Assert.assertEquals(EmergencyPage.getText(),expected_text );

	
		} catch (Exception e) {

			 System.out.println("Failed case");	
			 

			   	WebElement EmergencyPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Penalty']"));			
				String expected_text ="Penalty111";
				Assert.assertEquals(EmergencyPage.getText(),expected_text );
			
			}
	}
	

	/*
	 * First move to pending section than selct any slot time thanafter select any customer then finally click on Done button
	 */
	
	@Test(enabled=false,priority = 7, description = "verify that First move to pending section than selct any slot time thanafter select any customer then finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then finally click on Done button ")
	@Link("CD Partner without FnV version 8.6.apk")
	@Story("Story Name : clicking on Done button")
	public void delivery_Done() throws Exception {
		
		//select radamoly any slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on Done button
		pendingPage.click_on_Done_Button();
		
        //click on popup yes button
		pendingPage.click_on_Popup_Yes_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}


	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button
	 */
	
	@Test(enabled=false,priority = 8, description = "verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button ")
	@Story("Story Name : clicking on first done with remark button then after click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void delivery_Done_with_Remark() throws Exception {
		
		//seelct any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
		
		Thread.sleep(3000);
		
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on done button
		pendingPage.click_on_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
				
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button then finally click on done button
	 */
	
	@Test(enabled=false,priority = 9, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button then finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button then finally click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on future qty change button then after click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void delivery_Done_with_Remark_Future_Qty_Change() throws Exception {
		
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
				
		Thread.sleep(3000);
				
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on Future Qty Change
		pendingPage.Future_QtyChange_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
					
		
	}
	

	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button finally click on done button
	 */
	

	@Test(enabled=false,priority = 10, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button finally click on done button")
	@Story("Story Name : clicking on first done with remark button and click on future hold button finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Future_Hold_Button() throws Exception {
		
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
				
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on future hold button
		pendingPage.click_on_Future_Hold_Button();
	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future stop button
	 */
	
	@Test(enabled=false,priority = 11, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future stop button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future stop button ")
	@Story("Story Name : clicking on first done with remark button and click on Future  stop button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Future_Stop() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
										
		//click on continue button
		pendingPage.click_on_Continue_Button();

		//click on future stop
		pendingPage.click_on_Future_Stop_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}

	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button
	 */
	
	@Test(enabled=false,priority = 12, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button ")
	@Story("Story Name : clicking on first done with remark button and click on billing issue button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Billing_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
								
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on future stop
		pendingPage.click_on_Billing_Issue_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button
	 */

	@Test(enabled=false,priority = 13, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button ")
	@Story("Story Name : clicking on first done with remark button and click on call to customer button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void call_To_Customer() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
					
		//click on continue button
		pendingPage.click_on_Continue_Button();

		//click on future stop
		pendingPage.click_on_call_to_Customer_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button
	 */

	@Test(enabled=false,priority = 14, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on other button and enter text thanafter click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Other_Button_and_Done_Button() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
			
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		Thread.sleep(2000);
		//click on future stop
		pendingPage.click_on_Other_Button("Testing");
		
    	pendingPage.HideKeyword();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button
	 */

	@Test(enabled=false,priority = 15, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button ")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Today_Only() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
		//click on hold today button
		pendingPage.click_on_Hold_Today_Only_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button
	 */
	
	@Test(enabled=false,priority = 16, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button ")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Further() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
        //click on hold further button
		pendingPage.click_on_Hold_Further_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button
	 */

	@Test(enabled=false,priority = 17, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Quality() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on quality button
		pendingPage.click_on_stop_QualityButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	

	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button
	 */

	@Test(enabled=false,priority = 18, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_GoingOutOfStation() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on Going out of station button
		pendingPage.click_on_stop_GoingOutOfStationButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	
	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button
	 */

	@Test(enabled=false,priority = 19, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Timing() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on timing button
		pendingPage.click_on_stop_TimingButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	
	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button
	 */

	@Test(enabled=false,priority = 20, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Others() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on others button
		pendingPage.click_on_stop_OthersButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	


	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on click on Out of delivery area button
	 */

	@Test(enabled=false,priority = 21, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  click on Out of delivery area button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  click on Out of delivery area button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on click on Out of delivery area button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_OutOfDeliveryArea() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on Out of delivery area button
		pendingPage.click_on_OutOfDeliveryAreaButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button
	 */
	
	@Test(enabled=false,priority = 22, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hosue_Locked() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on house locked button		
		pendingPage.click_on_House_Locked_Button();
		
		//click on popup ok and continue button
		pendingPage.click_on_Ok_Continue_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 *First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on phone unreachable  button and again enter remark text than finally click on done button 
	 */
	
	@Test(enabled=false,priority = 23, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on phone unreachable  button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on phone unreachable button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on phone unreachable button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Phone_Unreachable() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on phone unreachable button
		pendingPage.click_on_Phone_Unreachable_Button();
		
		//click on popup ok and continue button
		pendingPage.click_on_Ok_Continue_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
	
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}

	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on multiple Ids button and  again enter remark text than finally click on done button
	 */
	
	@Test(enabled=false,priority = 24, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on multiple Ids button and  again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on multiple Ids button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on multiple Ids button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Mulitple_Ids() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on mulitiple Ids
		pendingPage.click_on_Mutiple_Ids_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on address issue button and  again enter remark text than finally click on done button
	 */

	@Test(enabled=false,priority = 25, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on address issue button and  again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on address issue button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on address issue button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Address_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on addres issue
		pendingPage.click_on_Address_Issue_Button();
				
		//click on popup ok and continue button
		pendingPage.click_on_Ok_Continue_Button();
			
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select mik leakage > enter otp and submit >enter remark and done. 
	 */

	@Test(enabled=false,priority = 26, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on milk leakage button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on milk leakage button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on milk leakage button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Milk_Leakage() throws Exception {
		
		try {
			Thread.sleep(2000);
			pendingPage.select_Randomly_any_Slot();
			
		    //click on not delivered
			pendingPage.click_on_Not_Delivered_Button();
			
			//click on our side
			pendingPage.click_on_Our_Side_Button();
			
			//click on milk leakage button
			pendingPage.clickonMilkLeakgeButton();

			//enter otp first then click on submit button
			pendingPage.EnterOTPandSubmit("11111");
			
			//click on remark section and enter remark text
			pendingPage.click_on_Remark_Section_Enter_Text();
			
			driver.hideKeyboard();
			//click on done button
			pendingPage.blue_Done_Button();
			
			String ActualResult=pendingPage.ActualResult();
			Assert.assertEquals(ActualResult, "Pending");
			
			Thread.sleep(8000);
			
		} catch (Exception e) {

			String ActualResult=pendingPage.ActualResult();
			Assert.assertEquals(ActualResult, "Pendingss");	
		
		}
	
}
	

	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select product shortage and also select the product >  >click on done. 
	 */
	

	@Test(enabled=false,priority = 27, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on product shortage button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on product shortage button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on product shortage button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Milk_Product_Shortage() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on product shortage button
		pendingPage.clickonProductShortageButton();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
				
		//select ramdomaly any products
		pendingPage.selectRandomllyAnyRadioButtonOfProducts();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}


	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select route late >enter otp is confirgured from admin and submit >enter remark and done. 
	 */
	
	
	@Test(enabled=false,priority = 28, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on route late button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on route late button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on route late button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Route_Late() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on milk leakage button
		pendingPage.clickonRouteLate_Button();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
			
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select Wrong Route >enter otp is confirgured from admin and submit >enter remark and done. 
	 */
	
	
	@Test(enabled=false,priority =29, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on wrong route button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on wrong route button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on wrong route  button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Wrong_Route() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on milk leakage button
		pendingPage.clickonWrongRouteButton();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");

		Thread.sleep(8000);
		
		
	}
		
	
	/*
	 * to validate that Cash with Billing Issue functionality
	 */
	
	@Test(enabled=false,priority = 30, description = "to validate that Cash with Billing Issue functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cash button and also enter Amount then finally click on Billing Issue button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cash button and also enter Amount then finally click on Billing Issue button")
	public void VerifyDoneWithRemarkwithCashForBillingIssue() throws Exception {
		
		Thread.sleep(6000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
	//	driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		cashPage.HideKeyword();
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cash button
		cashPage.click_on_cash_link();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		//click on billing Issue button
		cashPage.click_on_billingIssue_Button();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();

		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	/*
	 * to validate that Cheque with Do not have money change functionality
	 */

	@Test(enabled=false,priority = 31, description = "to validate that Cheque with Do not have money change functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cheque button and also enter Amount then finally click on Do not have money change button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cheque button and also enter Amount then finally click on Do not have money change button")
	public void VerifyDoneWithRemarkwithChequeforDonotHaveMoneyChange() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cheque button
		cashPage.click_on_cheque_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		cashPage.HideKeyword();
		
		Thread.sleep(2000);
		
		//click on Donot have money change button
		cashPage.click_on_DonotHaveMoneyChange_Button();
		
		//Enter cheque number text
		cashPage.enterChequeNumber();
		
		cashPage.HideKeyword();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	

	/*
	 * to validate that cash with Delivery Boy functionality
	 */
	
	@Test(enabled=false,priority = 32, description = "to validate that cash with Delivery Boy functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cash button and also enter Amount then finally click on Delivery Boy button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cash button and also enter Amount then finally click on Delivery Boy button")
	public void VerifyDoneWithRemarkForDeliveryBoy() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cash button
		cashPage.click_on_cash_link();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		driver.hideKeyboard();
		
		//click on delivery boy button
		cashPage.click_on_DeliveryBoy_Button();
		
		//click on Agent number radio button
		cashPage.click_on_AgentNumber_Button();
		
		//Enter number
		cashPage.enterMobileNumber();
		
		driver.hideKeyboard();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();

		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	

   /*
    * to validate that paid by delivery boy Cash functionality
   */
	
	@Test(enabled=false,priority = 33, description = "to validate that paid by delivery boy Cash functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on Delivery boy Cash button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on Delivery boy Cash button")
	public void VerifyNotColectedWithPaidByDeliveryBoyCash() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy cash button
		cashPage.click_on_deliveryBoyCash_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	/*
	 * to validate that paid by App functionality
	 */

	@Test(enabled=false,priority = 34, description = "to validate that paid by App functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on App button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on App button")
	public void VerifyNotCollected_Paid_App() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy app button
		cashPage.click_on_App_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
		
	/*
	 * to validate that paid by App functionality
	 */
	
	@Test(enabled=false,priority = 35, description = "to validate that paid by SMS Link functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on SMS Link button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on SMS Link button")
	public void VerifyNotCollected_Paid_SMSLink() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy SMS Link button
		cashPage.click_on_SMSLink_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}
	
	/*
	 * to validate that Will Pay By Paytm functionality
	 */
	
	@Test(enabled=false,priority = 36, description = "to validate that Will Pay By Paytm functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Will Pay By Paytm button and also select the date and finally click on Submit button")
	@Story("Story Name : verify that click on first Not collected button and click on Will Pay By Paytm button and also select the date and finally click on Submit button")
	public void VerifyNotCollected_Will_Pay_By_Paytm() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on will pay by paytm button
		cashPage.click_on_WillPayByPaytm_Button();
		
		Thread.sleep(2000);
		
	    // click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	/*
	 *to validate that Come Later functionality 
	 */
	
	@Test(enabled=false,priority = 37, description = "to validate that Come Later functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Come Later button and also select the date and finally click on Submit button")
	@Story("Story Name : verify that click on first Not collected button and click on Come Later button and also select the date and finally click on Submit button")
	public void VerifyNotCollected_ComeLater() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on come later button
		cashPage.click_on_ComeLater_Button();
		
		Thread.sleep(2000);
		
	    // click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	

	/*
	 *to validate that Billing Issue functionality 
	 */
	
	@Test(enabled=false,priority = 38, description = "to validate that Billing Issue functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Billing Issue button and finally click on proceed button")
	@Story("Story Name : verify that click on first Not collected button and click on Billing Issue button and finally click on proceed button")
	public void VerifyNotCollected_BillingIssue() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Billing issue button
		cashPage.click_on_billingIssue_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	/*
	 *to validate that Other functionality 
	 */
	
	@Test(enabled=false,priority = 39, description = "to validate that Other functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on other button and also enter text then finally click on submit button")
	@Story("Story Name : verify that click on first Not collected button and click on other button and also enter text then finally click on submit button")
	public void VerifyNotCollected_other() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on other button
		cashPage.click_on_other_Button();
		
		//click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	

	
	
	@AfterMethod(description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}
	
	
}
