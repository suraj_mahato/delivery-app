package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.PenaltyPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Penalty Page")
@Epic("End to End Testing")
public class PenaltyPageTest extends TestBase {
	
	PenaltyPage penaltyPage;

	public PenaltyPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		penaltyPage = new PenaltyPage();
		
		
	}
	
	
	@Test(priority = 0, description = "verify that Penalty Screen")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon thanafter click on penalty button")
	@Story("Story Name : verify that Penalty Screen")
	public void penalty_Page() throws Exception {
		

		 try {
			   penaltyPage.PenaltyScreen();

				WebElement PenaltyPage = driver.findElement(By.xpath("//android.widget.TextView[@text='This Month']"));			
				String expected_text ="This Month";
				Assert.assertEquals(PenaltyPage.getText(),expected_text );
	
		
			} catch (Exception e) {

				System.out.println("Failed cases");
			}
			
		}

/*
	@Test(enabled=false,priority = 1, description = "verify that first click on drawer icon and click on penalty button than click on previous Month link and again click on this month link ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon and click on penalty button than click on previous Month link and again click on this month link")
	@Story("Story Name : click on penalty button than click on previous Month link and again click on this month link")
	public void this_Month_Previou_Month_Page_Display() throws Exception {
		
		//click on drawer icon
		Thread.sleep(2000);
		penaltyPage.click_on_Drawer_Icon();
		
		//click on penalty button
		penaltyPage.click_on_Penalty_Button();
		
		//click on previous month link
		penaltyPage.click_on_Previous_Month_Link();
		
		//click on this month link
		penaltyPage.click_on_This_Month_Link();
		
		
		
		String Expected="Previous month page and this month page should display";
		String Actual="Previous month page and this month page should display";
		
		Assert.assertEquals(Actual, Expected);
		
		Thread.sleep(6000);
		
		
	}
	
	

	@Test(enabled=false,priority = 2, description = "verify that first click on drawer icon and click on penalty button than click on Rejected button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon and click on penalty button than click on Rejected button")
	@Story("Story Name : click on penalty button than click on Rejected button")
	public void rejected_Page() throws Exception {
		
		//click on drawer icon
		Thread.sleep(2000);
		penaltyPage.click_on_Drawer_Icon();
		
		//click on penalty button
		penaltyPage.click_on_Penalty_Button();
		
        //click on rejected button
		penaltyPage.click_on_Rejected_Button();
		
		String Actual_Result="Concern page should display";
		
		String Expected_Result="Concern page should display";
		
		Assert.assertEquals(Actual_Result, Expected_Result);
			
		Thread.sleep(5000);
		
		
	}
	
	@Test(enabled=false,priority = 3, description = "verify that first click on drawer icon and click on penalty button  than click on accepted button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon and click on penalty button  than click on acceptedbutton")
	@Story("Story Name : click on penalty button than  than click on accepted button")
	public void accepted_Page() throws Exception {
		
		//click on drawer icon
		Thread.sleep(2000);
		penaltyPage.click_on_Drawer_Icon();
		
		//click on penalty button
		penaltyPage.click_on_Penalty_Button();
		
        //click on rejected button
		penaltyPage.click_on_Accepted_Button();
		
		String Actual_Result="Concern page should display";
		
		String Expected_Result="Concern page should display";
		
		Assert.assertEquals(Actual_Result, Expected_Result);
			
		Thread.sleep(5000);
		
		
		
	}
	
	

	@Test(enabled=false,priority = 4, description = "verify that first click on drawer icon and click on penalty button  than click on Pending button and Enter valid remark text thanafter click on submit button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon and click on penalty button  than click on Pending button and Enter valid remark text thanafter click on submit button")
	@Story("Story Name : click on penalty button than than click on Pending button and Enter valid remark text thanafter click on submit button")
	public void pending_Page() throws Exception {
		
		//click on drawer icon
		Thread.sleep(2000);
		penaltyPage.click_on_Drawer_Icon();
		
		//click on penalty button
		penaltyPage.click_on_Penalty_Button();
		
        //click on rejected button
		penaltyPage.click_on_pending_Button("Testing");
		
		//click on submit button
		penaltyPage.click_on_Submit_Button();
		
		String Actual_Result="Penalty page should display";
		
		String Expected_Result="Penalty page should display";
		
		Assert.assertEquals(Actual_Result, Expected_Result);
			
		Thread.sleep(5000);
		
		
		
	}
	

	@Test(enabled=false,priority = 5, description = "verify that first click on drawer icon and click on penalty button  than click on Pending button and Enter valid remark text thanafter click on submit button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon and click on penalty button  than click on Pending button and Enter valid remark text thanafter click on submit button")
	@Story("Story Name : click on penalty button than than click on Pending button and Enter valid remark text thanafter click on submit button")
	public void pending_Page1() throws Exception {
		
		//click on drawer icon
		Thread.sleep(2000);
		penaltyPage.click_on_Drawer_Icon();
		
		//click on penalty button
		penaltyPage.click_on_Penalty_Button();
		
        //click on rejected button
		penaltyPage.click_on_pending_Button("");
		
		//click on submit button
		penaltyPage.click_on_Submit_Button();
		
		String Actual_Result="Enter Remark";
		
		Assert.assertTrue(true, Actual_Result);
		
		
		Thread.sleep(5000);
		
		
		
	}
	
	*/
	
	
	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End()  {
		if (driver != null) {
			driver.quit();

		}

	}
	

}
		