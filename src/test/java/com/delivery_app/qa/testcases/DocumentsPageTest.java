package com.delivery_app.qa.testcases;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.DocumentsPage;
import com.delivery_app.qa.pages.OldDocumentsPage;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Done , Done with remark and Not Collected Page")
@Epic("End to End Testing")
public class DocumentsPageTest extends TestBase {
	
	DocumentsPage documentsPage;
	

	public DocumentsPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		documentsPage= new DocumentsPage();
		
		
	}
	
	
	/**
	 * Test Cases
	 * 
	 * To validate that each and every pages of documents
	 * 
	 * 
	 */
		
	

	@Test(priority = 0, description = "to validate that Documents pages link only")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on each and every link like Accepted, pending")
	@Story("Story Name : verify that click on each and every link like Accepted, pending")
	public void VerifyDocumentPage() throws Exception {
		
		
		 try {
				documentsPage.Documents();

				WebElement DocumentPage = driver.findElement(By.xpath("//android.widget.TextView[@text='Please upload all the required document for uninterrupted app.']"));			
				String expected_text ="Please upload all the required document for uninterrupted app.";
				Assert.assertEquals(DocumentPage.getText(),expected_text );
	
		
			} catch (Exception e) {

				System.out.println("Failed cases");
			}
			
		}
		

	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}
}

