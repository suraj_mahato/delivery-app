package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.PendingPage;
import com.delivery_app.qa.utilities.FunctionClass;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Delivery: Done, Done with Remark and Not Delivered")
@Epic("End to End Testing")
public class PendingPageTest extends TestBase {
	
	PendingPage pendingPage;


	public PendingPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		pendingPage = new PendingPage();
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then finally click on Done button
	 */
	
	@Test(enabled=false,priority = 0, description = "verify that First move to pending section than selct any slot time thanafter select any customer then finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then finally click on Done button ")
	@Link("CD Partner without FnV version 8.6.apk")
	@Story("Story Name : clicking on Done button")
	public void delivery_Done() throws Exception {
		
		//select radamoly any slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on Done button
		pendingPage.click_on_Done_Button();
		
        //click on popup yes button
		pendingPage.click_on_Popup_Yes_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}


	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button
	 */
	
	@Test(enabled=false,priority = 1, description = "verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button ")
	@Story("Story Name : clicking on first done with remark button then after click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void delivery_Done_with_Remark() throws Exception {
		
		//seelct any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
		
		Thread.sleep(3000);
		
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on done button
		pendingPage.click_on_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
				
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button then finally click on done button
	 */
	
	@Test(enabled=false,priority = 2, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button then finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button then finally click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on future qty change button then after click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void delivery_Done_with_Remark_Future_Qty_Change() throws Exception {
		
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
				
		Thread.sleep(3000);
				
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on Future Qty Change
		pendingPage.Future_QtyChange_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
					
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button again change the days and also change end date then finally click on done button
	 */
	
	@Test(enabled=false,priority = 3, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button again change the days and also change end date then finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future qty change button again change the days and also change end date then finally click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on future qty chnage button, click change the days again change the End date then after click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Future_Qty_Change_Updating_Days_Duration() throws Exception {
		
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
				
		Thread.sleep(3000);
				
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on Future Qty Change
		pendingPage.Future_QtyChange_Button();
		
		//click on any radio button;
		pendingPage.click_Radio_Button();
		
		//click on Duration
		pendingPage.click_Duration_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
				
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button finally click on done button
	 */
	

	@Test(enabled=false,priority = 4, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button finally click on done button")
	@Story("Story Name : clicking on first done with remark button and click on future hold button finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Future_Hold_Button() throws Exception {
		
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
				
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on future hold button
		pendingPage.click_on_Future_Hold_Button();
	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button and also update end date than finally click on done button
	 */

	@Test(enabled=false,priority = 5, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button and also update end date than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future hold button and also update end date than finally click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on future hold button and also update end date than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Future_Hold_Button_select_End_Date() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();

		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on future hold button
		pendingPage.click_on_Future_Hold_Button();
		
		//click on Duration
		pendingPage.click_Duration_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future stop button
	 */
	
	@Test(enabled=false,priority = 6, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future stop button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on Future stop button ")
	@Story("Story Name : clicking on first done with remark button and click on Future  stop button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Future_Stop() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
										
		//click on continue button
		pendingPage.click_on_Continue_Button();

		//click on future stop
		pendingPage.click_on_Future_Stop_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on quality issue button
	 */

	@Test(enabled=false,priority = 7, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on quality issue button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on quality issue button ")
	@Story("Story Name : clicking on first done with remark button and click on quality issue button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Quality_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
									
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on future stop
		pendingPage.click_on_Quality_Issue_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button
	 */
	
	@Test(enabled=false,priority = 8, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button ")
	@Story("Story Name : clicking on first done with remark button and click on billing issue button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Billing_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
								
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on future stop
		pendingPage.click_on_Billing_Issue_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button
	 */

	@Test(enabled=false,priority = 9, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button ")
	@Story("Story Name : clicking on first done with remark button and click on call to customer button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void call_To_Customer() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
					
		//click on continue button
		pendingPage.click_on_Continue_Button();

		//click on future stop
		pendingPage.click_on_call_to_Customer_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button
	 */

	@Test(enabled=false,priority = 10, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on other button and enter text thanafter click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Other_Button_and_Done_Button() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		pendingPage.click_on_Done_with_Remark_Button();
			
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		Thread.sleep(2000);
		//click on future stop
		pendingPage.click_on_Other_Button("Testing");
		
    	pendingPage.HideKeyword();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button
	 */

	@Test(enabled=false,priority = 11, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button ")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today only button finally click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Today_Only() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
		//click on hold today button
		pendingPage.click_on_Hold_Today_Only_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today and change qty button finally click on Done button
	 */

	@Test(enabled=false,priority = 12, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today and change qty button finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today  and change qty button finally click on Done button ")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today  and change qty button finally click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Today_Change_Qty() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
		//click on hold today and change quantity
		pendingPage.click_on_Hold_Today_And_Change_Quantity_Button();	
		
		//click on done button
		pendingPage.blue_Done_Button();
	
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today and change qty button and update the qty for new product and change the days and durations finally click on Done button
	 */

	@Test(enabled=false,priority = 13, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today and change qty button and update the qty for new product and change the days and durations finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today  and change qty button and update the qty for new product and change the days and durations finally click on Done button ")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold Today  and change qty button  and update the qty for new product and change the days and durations finally click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Today_Change_Qty_Chnage_Day_Change_End_Date() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
		//click on hold today and change quantity
		pendingPage.click_on_Hold_Today_And_Change_Quantity_Button();	
		
		//click on alterate days
		pendingPage.click_Radio_Button();
		
		//click on end date
		pendingPage.click_Duration_Button();
			
		//click on done button
		pendingPage.blue_Done_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button
	 */
	
	@Test(enabled=false,priority = 14, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button ")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and finally click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Further() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
        //click on hold further button
		pendingPage.click_on_Hold_Further_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and update the end date and finally click on Done button again enter remark text than finally click on done button
	 */

	@Test(enabled=false,priority = 15, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and update the end date and finally click on Done button again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and update the end date and finally click on Done button again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on hold button again click on Hold further button and update the end date and finally click on Done button again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hold_Further_Change_End_Date() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on Hold Button
		pendingPage.click_on_Hold_Button();
		
        //click on hold further button
		pendingPage.click_on_Hold_Further_Button();
		
		//click on end date
		pendingPage.click_Duration_Button();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button
	 */

	@Test(enabled=false,priority = 16, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on quality button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Quality() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on quality button
		pendingPage.click_on_stop_QualityButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	

	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on services button
	 */

	@Test(enabled=false,priority = 17, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on services button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on services button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on services button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Service() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on services button
		pendingPage.click_on_stop_ServiceButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button
	 */

	@Test(enabled=false,priority = 18, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Going out of station button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_GoingOutOfStation() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on Going out of station button
		pendingPage.click_on_stop_GoingOutOfStationButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on billing button
	 */

	@Test(enabled=false,priority = 19, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on billing button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on billing button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on billing button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Billing() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on billing button
		pendingPage.click_on_stop_BillingButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	
	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button
	 */

	@Test(enabled=false,priority = 20, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on timing button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Timing() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on timing button
		pendingPage.click_on_stop_TimingButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	
	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button
	 */

	@Test(enabled=false,priority = 21, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on others button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_Others() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on others button
		pendingPage.click_on_stop_OthersButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	

	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on Only For trail button
	 */

	@Test(enabled=false,priority = 22, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  Only For trail button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  Only For trail button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  Only For trail button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_OnlyForTrail() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on  Only For trail button
		pendingPage.click_on_stop_OnlyForTrailButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on click on Not Required button
	 */

	@Test(enabled=false,priority = 23, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  click on Not Required button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  click on Not Required button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on click on Not Required button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_NotRequired() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on Not Required button
		pendingPage.click_on_NotRequiredButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	/*
	 * First move to pending section than select any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on click on Out of delivery area button
	 */

	@Test(enabled=false,priority = 24, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  click on Out of delivery area button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on  click on Out of delivery area button")
	@Story("Story Name : First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on stop button and finally click on click on Out of delivery area button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Stop_OutOfDeliveryArea() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on stop button		
		pendingPage.click_on_stop_Button();
		
		//click on Out of delivery area button
		pendingPage.click_on_OutOfDeliveryAreaButton();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button
	 */
	
	@Test(enabled=false,priority = 25, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on house locked button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Hosue_Locked() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on house locked button		
		pendingPage.click_on_House_Locked_Button();
		
		//click on popup ok and continue button
		pendingPage.click_on_Ok_Continue_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 *First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on phone unreachable  button and again enter remark text than finally click on done button 
	 */
	
	@Test(enabled=false,priority = 26, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on phone unreachable  button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on phone unreachable button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on phone unreachable button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Phone_Unreachable() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on phone unreachable button
		pendingPage.click_on_Phone_Unreachable_Button();
		
		//click on popup ok and continue button
		pendingPage.click_on_Ok_Continue_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
	
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on quality issue button and again enter remark text than finally click on done button
	 */
	
	@Test(enabled=false,priority = 27, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on quality issue button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on quality issue button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on quality issue button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Quality_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on quality issue
		pendingPage.click_on_Quality_Issue_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on billing issue button and again enter remark text than finally click on done button
	 */

	@Test(enabled=false,priority = 28, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on billing issue button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on billing issue button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on billing issue button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Billing_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on billing issue
		pendingPage.click_on_Billing_Issue_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on multiple Ids button and  again enter remark text than finally click on done button
	 */
	
	@Test(enabled=false,priority = 29, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on multiple Ids button and  again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on multiple Ids button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on multiple Ids button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Mulitple_Ids() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on mulitiple Ids
		pendingPage.click_on_Mutiple_Ids_Button();
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	
	/*
	 * First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on address issue button and  again enter remark text than finally click on done button
	 */

	@Test(enabled=false,priority = 30, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on address issue button and  again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on customer side button than click on address issue button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on customer side button than click on address issue button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Customer_Side_Address_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on customer side button
		pendingPage.click_on_Customer_Side_Button();
		
		//click on addres issue
		pendingPage.click_on_Address_Issue_Button();
				
		//click on popup ok and continue button
		pendingPage.click_on_Ok_Continue_Button();
			
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select mik leakage > enter otp and submit >enter remark and done. 
	 */

	@Test(enabled=false,priority = 31, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on milk leakage button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on milk leakage button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on milk leakage button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Milk_Leakage() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();
		
		//click on milk leakage button
		pendingPage.clickonMilkLeakgeButton();

		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();	
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	

	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select product shortage and also select the product >  >click on done. 
	 */
	

	@Test(enabled=false,priority = 32, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on product shortage button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on product shortage button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on product shortage button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Milk_Product_Shortage() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on product shortage button
		pendingPage.clickonProductShortageButton();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
				
		//select ramdomaly any products
		pendingPage.selectRandomllyAnyRadioButtonOfProducts();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}


	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select route late >enter otp is confirgured from admin and submit >enter remark and done. 
	 */
	
	
	@Test(enabled=false,priority = 33, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on route late button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on route late button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on route late button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Route_Late() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on milk leakage button
		pendingPage.clickonRouteLate_Button();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
			
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select out of delivery area >enter otp is confirgured from admin and submit >enter remark and done. 
	 */

	@Test(enabled=false,priority = 34, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on out of Delivery area button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on out of Delivery area button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on out of Delivery area button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Out_Of_Delivery_Area() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on milk leakage button
		pendingPage.clickonOutOfDeliveryAreaButton();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");

		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		
		//click on done button
		Thread.sleep(2000);
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");
		
		Thread.sleep(8000);
		
		
	}
	
	/*
	 * select any timeslot first > customer > Not delivered button > our side button >select Wrong Route >enter otp is confirgured from admin and submit >enter remark and done. 
	 */
	
	
	@Test(enabled=false,priority =35, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on wrong route button and again enter remark text than finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Not Delivered button again click on our side button than click on wrong route button and again enter remark text than finally click on done button")
	@Story("Story Name : click on Not Delivered button again click on our side button than click on wrong route  button and again enter remark text than finally click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Not_Delivered_Our_Side_Wrong_Route() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
	    //click on not delivered
		pendingPage.click_on_Not_Delivered_Button();
		
		//click on our side
		pendingPage.click_on_Our_Side_Button();

		//click on milk leakage button
		pendingPage.clickonWrongRouteButton();
		
		//enter otp first then click on submit button
		pendingPage.EnterOTPandSubmit("11111");
		
		//click on remark section and enter remark text
		pendingPage.click_on_Remark_Section_Enter_Text();
		
		pendingPage.HideKeyword();
		
		//click on done button
		pendingPage.blue_Done_Button();
		
		String ActualResult=pendingPage.ActualResult();
		Assert.assertEquals(ActualResult, "Pending");

		Thread.sleep(8000);
		
		
	}
	
	

	@AfterMethod(description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}
	
	
}
		
