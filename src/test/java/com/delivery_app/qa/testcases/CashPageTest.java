package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.CashPage;
import com.delivery_app.qa.pages.PenaltyPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

@Listeners({DeliveryAppListeners.class})
@Feature("Done , Done with remark and Not Collected Page")
@Epic("End to End Testing")
public class CashPageTest extends TestBase {
	
	CashPage cashPage;

	public CashPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		cashPage= new CashPage();
		
		
	}
	
	
	/**
	 * Test Cases
	 * 
	 * To validate that each and every Done, Done with Remark and Not collected Functionality cases like
	 * 
	 *  Done
	 *  Done with Remark filed
	 *  - Card
	 *  - Cheque
	 *  - Cash
	 *  - Pay with Paytm Barcode
	 *  -UPI/QR
	 *  Billing Issue
	 *  Done't have money change
	 *  Quality Issue
	 *  Delivery Boy
	 *  Call to Customer
	 * 
	 *  Paid filed
	 * -Delivery Boy Cash
	 * - Delivery Boy Cheque
	 * - App
	 * - neft
	 * - SMS link
	 * 
	 * Will Pay by Paytam
	 * Will Pay by App
	 * Come Later
	 * House Locked
	 * Send Bill
	 * Billing Issue
	 * Other
	 * 
	 * 
	 */
		
	

	@Test(priority = 0, description = "to validate that Cash with Billing Issue functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cash button and also enter Amount then finally click on Billing Issue button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cash button and also enter Amount then finally click on Billing Issue button")
	public void VerifyDoneWithRemarkwithCashForBillingIssue() throws Exception {
		
		Thread.sleep(6000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
	//	driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		cashPage.HideKeyword();
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cash button
		cashPage.click_on_cash_link();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		//click on billing Issue button
		cashPage.click_on_billingIssue_Button();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();

		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	

	@Test(enabled=false,priority = 1, description = "to validate that Cheque with Do not have money change functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cheque button and also enter Amount then finally click on Do not have money change button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cheque button and also enter Amount then finally click on Do not have money change button")
	public void VerifyDoneWithRemarkwithChequeforDonotHaveMoneyChange() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cheque button
		cashPage.click_on_cheque_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		cashPage.HideKeyword();
		
		Thread.sleep(2000);
		
		//click on Donot have money change button
		cashPage.click_on_DonotHaveMoneyChange_Button();
		
		//Enter cheque number text
		cashPage.enterChequeNumber();
		
		cashPage.HideKeyword();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 2, description = "to validate that Cheque with Quality Issue functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cheque button and also enter Amount then finally click on Quality Issue button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cheque button and also enter Amount then finally click on Quality Issue button")
	public void VerifyDoneWithRemarkForQualityIssue() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button

    	Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cheque button
		cashPage.click_on_cheque_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		cashPage.HideKeyword();
		
		//click on Quality Issue button
		cashPage.click_on_QualityIssue_Button();
		
		//Enter cheque number text
		cashPage.enterChequeNumber();
		
		cashPage.HideKeyword();
		
		//click on Relative number radio button
		cashPage.click_on_RelativeNumber_Button();
		
		//Enter number
		cashPage.enterMobileNumber();
		
		cashPage.HideKeyword();
		
		
		//click on Request button
		cashPage.click_on_Request_Button();

		
		//click on proceed button
		cashPage.click_on_proceed_Button();
	
		//Enter OTP number
    	cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 3, description = "to validate that cash with Delivery Boy functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cash button and also enter Amount then finally click on Delivery Boy button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cash button and also enter Amount then finally click on Delivery Boy button")
	public void VerifyDoneWithRemarkForDeliveryBoy() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Done with remark button
		Thread.sleep(2000);
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cash button
		cashPage.click_on_cash_link();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		driver.hideKeyboard();
		
		//click on delivery boy button
		cashPage.click_on_DeliveryBoy_Button();
		
		//click on Agent number radio button
		cashPage.click_on_AgentNumber_Button();
		
		//Enter number
		cashPage.enterMobileNumber();
		
		driver.hideKeyboard();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();

		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 4, description = "to validate that cheque with call To Customer functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Done with remark button and click on Cheque button and also enter Amount then finally click on call To Customer button")
	@Story("Story Name : verify that click on first Done with remark button and click on Cheque button and also enter Amount then finally click on call To Customer button")
	public void VerifyDoneWithRemarkForCallToCustomer() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Done with remark button
		cashPage.click_on_DoneWithRemark_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Cheque button
		cashPage.click_on_cheque_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // Enter Amount
		cashPage.enterAmount();
		
		driver.hideKeyboard();
		
		//click on call to customer button
		cashPage.click_on_CallToCustomer_Button();
		
		//Enter cheque number text
		cashPage.enterChequeNumber();
				
		driver.hideKeyboard();
		
		//click on Agent number radio button
		cashPage.click_on_AgentNumber_Button();
		
		//Enter number
		cashPage.enterMobileNumber();
		
		driver.hideKeyboard();
		
		//click on Request button
		cashPage.click_on_Request_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		//Enter OTP number
		cashPage.enterOTP();
		
		//click on verify otp button
		cashPage.click_on_VerifyOTP_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}
	
	
	
	@Test(enabled=false,priority = 5, description = "to validate that paid by delivery boy Cash functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on Delivery boy Cash button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on Delivery boy Cash button")
	public void VerifyNotColectedWithPaidByDeliveryBoyCash() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy cash button
		cashPage.click_on_deliveryBoyCash_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 6, description = "to validate that paid by delivery boy Cheque functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on Delivery boy Cash button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on Delivery boy Cash button")
	public void NotCollected_Paid_DeliveryBoyCheque() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy cheque button
		cashPage.click_on_deliveryBoyCheque_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	

	@Test(enabled=false,priority = 7, description = "to validate that paid by App functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on App button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on App button")
	public void NotCollected_Paid_App() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy app button
		cashPage.click_on_App_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 8, description = "to validate that paid by NEFT functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on NEFT button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on NEFT button")
	public void NotCollected_Paid_NEFT() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy NEFT button
		cashPage.click_on_NEFT_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 9, description = "to validate that paid by SMS Link functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Paid button and finally click on SMS Link button")
	@Story("Story Name : verify that click on first Not collected button and click on Paid button and finally click on SMS Link button")
	public void NotCollected_Paid_SMSLink() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Paid button
		cashPage.click_on_Paid_Button();
		
		String expectedResultCust = "process list should be display ";
		String ActualResultCust= "process list should be display ";
		Assert.assertEquals(ActualResultCust, expectedResultCust, " process list not display");
		
	    // click on Delivery boy SMS Link button
		cashPage.click_on_SMSLink_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 10, description = "to validate that Will Pay By Paytm functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Will Pay By Paytm button and also select the date and finally click on Submit button")
	@Story("Story Name : verify that click on first Not collected button and click on Will Pay By Paytm button and also select the date and finally click on Submit button")
	public void NotCollected_Will_Pay_By_Paytm() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on will pay by paytm button
		cashPage.click_on_WillPayByPaytm_Button();
		
		Thread.sleep(2000);
		
	    // click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 11, description = "to validate that Will Pay By App functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Will Pay By App button and also select the date and finally click on Submit button")
	@Story("Story Name : verify that click on first Not collected button and click on Will Pay By App button and also select the date and finally click on Submit button")
	public void NotCollected_Will_Pay_By_App() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on will pay by app button
		cashPage.click_on_WillPayApp_Button();
		
		Thread.sleep(2000);
		
	    // click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 12, description = "to validate that Come Later functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Come Later button and also select the date and finally click on Submit button")
	@Story("Story Name : verify that click on first Not collected button and click on Come Later button and also select the date and finally click on Submit button")
	public void NotCollected_ComeLater() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on come later button
		cashPage.click_on_ComeLater_Button();
		
		Thread.sleep(2000);
		
	    // click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 13, description = "to validate that House Locked functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on House Locked button and finally click on proceed button")
	@Story("Story Name : verify that click on first Not collected button and click on House Locked button and finally click on proceed button")
	public void NotCollected_HouseLocked() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on House locked button
		cashPage.click_on_HouseLocked_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 14, description = "to validate that Billing Issue functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on Billing Issue button and finally click on proceed button")
	@Story("Story Name : verify that click on first Not collected button and click on Billing Issue button and finally click on proceed button")
	public void NotCollected_BillingIssue() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on Billing issue button
		cashPage.click_on_billingIssue_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	
	@Test(enabled=false,priority = 15, description = "to validate that Other functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first Not collected button and click on other button and also enter text then finally click on submit button")
	@Story("Story Name : verify that click on first Not collected button and click on other button and also enter text then finally click on submit button")
	public void NotCollected_other() throws Exception {
		
		Thread.sleep(3000);
		//click on cash link
		cashPage.click_on_cash_link();
		
		Thread.sleep(8000);
		
		//select randomly any customer
		cashPage.select_Randomly_any_Customer();
		
		//click on Open app setting button
		cashPage.click_on_OpenAppSetting_Button();
		
		// click on mobile OS Back button
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//Click on Not collected button
		Thread.sleep(2000);
		cashPage.click_on_NotCollected_Button();
	
		
		String expectedResult = "payment process list should be display ";
		String ActualResult= "payment process list should be display ";
		Assert.assertEquals(ActualResult, expectedResult, "Payment process list not display");
		
		//click on other button
		cashPage.click_on_other_Button();
		
		//click on submit button
		cashPage.click_on_submit_Button();
		
		String expectedResult1 = "Confirmation popup message should be display ";
		String ActualResult1= "Confirmation popup message should be display ";
		Assert.assertEquals(ActualResult1, expectedResult1, "Confirmation popup message not display");
		
		//click on proceed button
		cashPage.click_on_proceed_Button();
		
		String expectedResult2 = "Action complted popup message display first and also navigate back to cash page ";
		String ActualResult2= "Action complted popup message display first and also navigate back to cash page ";
		Assert.assertEquals(ActualResult2, expectedResult2, "cash page not display");
		
		Thread.sleep(6000);
		
	}	
	

	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}
}
