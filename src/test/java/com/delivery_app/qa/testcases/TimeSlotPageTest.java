package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.OldTimeSlotPage;
import com.delivery_app.qa.pages.TimeSlotPage;
import com.relevantcodes.extentreports.LogStatus;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("TimeSlot Page")
@Epic("End to End Testing")
public class TimeSlotPageTest extends TestBase {
	
	TimeSlotPage timeSlotPage;

	public TimeSlotPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		timeSlotPage = new TimeSlotPage();
		
		
	}
	
	
	@Test(priority = 0, description = "To Validate that clicking one by one Time Slots.")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: In Home Screen,clicking one by one Time Slots. ")
	@Story("Story Name : clicking one by one Time Slots.")
	public void VerifyTimeslot() throws Exception {
	
		
		try {
		
			timeSlotPage.clickonTimeSlotIcon();
			timeSlotPage.clickonTimeSlotIcon();
			timeSlotPage.TimeSlot();
			timeSlotPage.clickonTimeSlotIcon();
	//		Assert.assertTrue(timeSlotPage.Result);
			
		
			
			Thread.sleep(5000);
			
		} catch (Exception e) {
		
			
		}
	
	/*	
		String expectedResult2 = "It should be display list of all deliveries";
		String ActualResult2= "It should be display list of all deliveries";
		Assert.assertEquals(ActualResult2, expectedResult2, "list if deliveries not display");
		*/
		
		
		
	}
	
	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}
	}
	
	
}


