package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.OldScanDemandPage;
import com.delivery_app.qa.pages.OldSearchPage;
import com.delivery_app.qa.pages.SearchPage;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Seeach Screen")
@Epic("End to End Testing")
public class SearchPageTest extends TestBase {
	
	SearchPage searchPage;

	public SearchPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		searchPage = new SearchPage();
		
		
	}
	
	
	@Test(priority = 0, description = "verify that first click on randomly any radio button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on randomly any radio button")
	@Story("Story Name : click on randomly any radio button")
	public void VerifysearchPage() throws Exception {
		
		
		 try {
				searchPage.Search();
				Assert.assertTrue(searchPage.Result);
				
				Thread.sleep(3000);
				
			} catch (Exception e) {

			
			}
			
		}
		
		
		
	/*	
		
		//click on drawer icon
	    searchPage.click_on_Search_link();
	    
	    //select randomly any radio search  button
	    searchPage.select_Randomly_any_searchRadio_Button();  
	    
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		
		*/
	
		
		
	}
	
	
	
	/*
	
	@Test(priority = 1, description = "verify that first click on randomly any radio button thenafter search any customer with customer Id")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on randomly any radio button thenafter search any customer with customer Id")
	@Story("Story Name : click on randomly any radio button thenafter search any customer with customer Id")
	public void searchinganyCustomerwithCustomerId() throws Exception {
		
		//click on drawer icon
	    searchPage.click_on_Search_link();
	    
	    //select randomly any radio search  button
	    searchPage.select_Randomly_any_searchRadio_Button();  
	    
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		Thread.sleep(3000);
		
		searchPage.click_on_SearchFiled_link("495296");
		
		Thread.sleep(3000);
		
	}
	
	@Test(priority = 2, description = "verify that first click on randomly any radio button thenafter search any customer with customer Name")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on randomly any radio button thenafter search any customer with customer Name")
	@Story("Story Name : click on randomly any radio button thenafter search any customer with customer Name")
	public void searchinganyCustomerwithCustomerName() throws Exception {
		
		//click on drawer icon
	    searchPage.click_on_Search_link();
	    
	    //select randomly any radio search  button
	    searchPage.select_Randomly_any_searchRadio_Button();  
	    
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		Thread.sleep(3000);
		
		searchPage.click_on_SearchFiled_link("pradeep");
		
		Thread.sleep(3000);
		
	}
	
	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}
	}
	
	
	*/
	


