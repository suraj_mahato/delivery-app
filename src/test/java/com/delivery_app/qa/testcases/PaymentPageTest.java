package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.PaymentPage;
import com.relevantcodes.extentreports.LogStatus;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Penalty Page")
@Epic("End to End Testing")
public class PaymentPageTest extends TestBase {
	
	PaymentPage paymentPage;

	public PaymentPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		paymentPage = new PaymentPage();
		
		
	}
	
	
	@Test(priority = 0, description = "verify that first click on drawer icon thanafter click on payment link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon thanafter click on payment link")
	@Story("Story Name : click on payment link")
	public void verifyPaymentPage() throws Exception {

	 try {
			paymentPage.Payments();
			Assert.assertTrue(paymentPage.Result);

			
			
		} catch (Exception e) {
			
			test.log(LogStatus.FAIL, "Payment  Failed");

			
		}
		
	}		

	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}
	}
	
	
}

