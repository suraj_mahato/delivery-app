package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.StarPerformerPage;
import com.delivery_app.qa.pages.OldTimeSlotPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ DeliveryAppListeners.class })
@Feature("Star Performance Page")
@Epic("End to End Testing")
public class StarPerformerPageTest extends TestBase {

	StarPerformerPage starPerformerPage;

	public StarPerformerPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();

		starPerformerPage = new StarPerformerPage();

	}

	@Test(priority = 0, description = "To Validate that clicking on star performanc field")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: In Drawer Screen,clicking on star performanc field ")
	@Story("Story Name : clicking on star performanc field ")
	public void starPerformancePage() throws Exception {

		//click on drawer icon
		starPerformerPage.StarPerformer();

		Thread.sleep(3000);

	}
	

	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}
	}
	
	
}

