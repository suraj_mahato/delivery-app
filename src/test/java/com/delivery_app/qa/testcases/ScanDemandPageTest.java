package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.OldScanDemandPage;
import com.delivery_app.qa.pages.ScanDemandPage;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Penalty Page")
@Epic("End to End Testing")
public class ScanDemandPageTest extends TestBase {
	
	ScanDemandPage scanDemandPage;

	public ScanDemandPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		scanDemandPage = new ScanDemandPage();
		
		
	}
	
	
	@Test(priority = 0, description = "verify that first click on drawer icon thanafter click on scan demand link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on drawer icon thanafter click on scan demand link")
	@Story("Story Name : click on scan demand link")
	public void VerifyScanDemandPage() throws Exception {
	
		

		 try {
				scanDemandPage.ScanDemand();
				Assert.assertTrue(scanDemandPage.Result);
				Thread.sleep(3000);
				
			} catch (Exception e) {

				System.out.println("Failed cases");
			}
		 
			
			
		}		
	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}
	}
	
	
}
