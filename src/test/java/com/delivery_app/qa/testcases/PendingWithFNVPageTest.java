package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.PendingPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Delivery: Done, Done with Remark and Not Delivered")
@Epic("End to End Testing")
public class PendingWithFNVPageTest extends TestBase {
	
	PendingPage pendingPage;

	public PendingWithFNVPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		pendingPage = new PendingPage();
		
		
	}
	
	
	@Test(enabled=false,priority = 0, description = "verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button then finally click on done button ")
	@Story("Story Name : clicking on first done with remark button then after click on Done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void delivery_Done() throws Exception {
		
		//select radamoly any slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
		
		Thread.sleep(3000);
		
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		//click on done button
		pendingPage.click_on_Done_Button();
		Thread.sleep(8000);
		
		
		
	}
	
	@Test(enabled=false,priority = 1, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on quality issue button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on quality issue button ")
	@Story("Story Name : clicking on first done with remark button and click on quality issue button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Quality_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
						
		Thread.sleep(3000);
						
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		Thread.sleep(2000);
		//click on future stop
		pendingPage.click_on_Quality_Issue_Button();
		
		Thread.sleep(8000);
		
		
	}
	
	@Test(enabled=false,priority = 2, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on billing issue button ")
	@Story("Story Name : clicking on first done with remark button and click on billing issue button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Billing_Issue() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
								
		Thread.sleep(3000);
								
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		Thread.sleep(2000);
		//click on future stop
		pendingPage.click_on_Billing_Issue_Button();
		
		Thread.sleep(8000);
		
		
	}
	
	@Test(enabled=false,priority = 3, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on call to customer button ")
	@Story("Story Name : clicking on first done with remark button and click on call to customer button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void call_To_Customer() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
								
		Thread.sleep(3000);
								
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		Thread.sleep(2000);
		//click on future stop
		pendingPage.click_on_call_to_Customer_Button();
		
		Thread.sleep(8000);
		
		
	}

	@Test(priority = 4, description = "Verify that First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: First move to pending section than selct any slot time thanafter select any customer then click on Done with Remark button and click on other button and enter text thanafter click on done button ")
	@Story("Story Name : clicking on first done with remark button and click on other button and enter text thanafter click on done button")
	@Link("CD Partner without FnV version 8.6.apk")
	public void Other_Button_and_Done_Button() throws Exception {
		 
		//select any time slot
		Thread.sleep(2000);
		pendingPage.select_Randomly_any_Slot();
		
		//click on done with remark button
		Thread.sleep(2000);
		pendingPage.click_on_Done_with_Remark_Button();
								
		Thread.sleep(3000);
								
		//click on continue button
		pendingPage.click_on_Continue_Button();
		
		Thread.sleep(2000);
		//click on future stop
		pendingPage.click_on_Other_Button("Testing");
		
		driver.hideKeyboard();
		
		//click on done button
		Thread.sleep(2000);
		pendingPage.blue_Done_Button();
		
		Thread.sleep(8000);
		
		
		
	}
	

}
