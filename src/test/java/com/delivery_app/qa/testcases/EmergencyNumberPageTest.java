package com.delivery_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.listeners.DeliveryAppListeners;
import com.delivery_app.qa.pages.EmergencyNumberPage;
import com.delivery_app.qa.pages.OldEmergencyNumberPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({DeliveryAppListeners.class})
@Feature("Emergency Number Page")
@Epic("End to End Testing")
public class EmergencyNumberPageTest extends TestBase {
	
	EmergencyNumberPage emergencyNumber;

	public EmergencyNumberPageTest() throws IOException {
		super();

	}

	@BeforeMethod (description = "Andriod Devices Set Up")
	public void setUp() throws Exception {

		initiallization();
		
		emergencyNumber = new EmergencyNumberPage();
		
		
	}
	
	
	/**
	 * Test Cases
	 * 
	 * To validate that valid and invalid first contact number and second contact number filed for Delivery Boy (User).
	 * 
	 * 
	 */
		
	

	@Test( description = "to validate that Enter valid First contact Number")
	@Severity(SeverityLevel.BLOCKER)
	@Link("CD Partner without FnV version 8.6.apk")
	@Description("Test Case Description: click on first contact number and Enter valid First contact Number")
	@Story("Story Name : verify that click on first contact number and Enter valid First contact Number")
	public void VerifyEmergencyNumber() throws Exception {	
				
				
				 try {
						emergencyNumber.EmergencyNumber();
						Assert.assertTrue(emergencyNumber.Result);	
							
						
					} catch (Exception e) {
						
				//		test.log(LogStatus.FAIL, "emergency number  Failed");

						
						
					}
					
				}	
	

		
	
	
	
	@AfterMethod (description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}
	}
	
	
}
