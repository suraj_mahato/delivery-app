package com.delivery_app.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.android.AndroidDriver;

public class TestBase {

	
	public static ThreadLocal<AndroidDriver> tdriver = new ThreadLocal<AndroidDriver>();

	public static AndroidDriver driver;
	public static Properties prop;
	protected static ExtentTest test;
	protected static ExtentReports report;
	public static Properties Config=new Properties();
	public static FileInputStream input;	


	public Boolean Result;
	public static Properties prop_OR;



	public TestBase() {
		
	
		try {

			prop = new Properties();
		
			FileInputStream ip = new FileInputStream(
					"/home/dell/eclipse-workspace/delivery-app"
                                 + "/src/main/resources/properties/Config.properties");
						
			prop.load(ip);	
			
		} catch (FileNotFoundException e) {
			System.out.println("File Not found");
			e.printStackTrace();

		} catch (IOException e) {
			System.out.println("Io Exception");
			e.printStackTrace();

		}
	}

	
	/** Setting OR properties file code */
	public void setORprop() throws Exception {

		try {

			Result = false;
			prop_OR = new Properties();

			input = new FileInputStream(
					"/home/dell/eclipse-workspace/delivery-app" + "/src/main/resources/properties/OR.properties");

			if (input != null) {
				prop_OR.load(input);

			} else {
				throw new FileNotFoundException("Property File" + input + "not found in the classpath");
			}

			Result = true;

		} catch (Exception e) {
			Reporter.log("Exception" + e);
		}
	}	
	
	
	/** Setting up the Environment to start the Test */
	public AndroidDriver initiallization() throws MalformedURLException {
		
		
		File app_lac = new File(prop.getProperty("appPath"));
    	File app = new File(app_lac, prop.getProperty("application")); 
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("device", prop.getProperty("deviceA"));
		caps.setCapability("automationName", prop.getProperty("automationName1"));
		caps.setCapability("deviceName", prop.getProperty("deviceNameR"));	
		caps.setCapability("udid", prop.getProperty("udidR"));
		caps.setCapability("platformName", prop.getProperty("platformNameA"));
		caps.setCapability("app", app.getAbsolutePath());
		caps.setCapability("autoGrantPermission", true);
		caps.setCapability("skipDeviceInitialization", true);
		caps.setCapability("skipServerInstallation", true);
		caps.setCapability("noReset", true);
		
		
		caps.setCapability("appPackage", prop.getProperty("appPackage1"));
		caps.setCapability("appWaitActivity", prop.getProperty("appWaitActivity"));

     	driver = new AndroidDriver(new URL(prop.getProperty("appiumServer")), caps);
		
//		driver = new AndroidDriver(new URL("http://127.0.0.1:"+prop.getProperty("port")+"/wd/hub"), caps);
	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		tdriver.set(driver);
		
		return getDriver();

		}
		
	

	public static synchronized AndroidDriver getDriver() {
		return tdriver.get();
	}

}

	
	

