package com.delivery_app.qa.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.delivery_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Step;

public class FunctionClass extends TestBase {
	
	

	/** Method used to wait until element is found */
	public static void waitUntilFound(String xpath, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		try {
			if (xpath != null) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element or Xpath not found on page: </font><br>" + e);
		}
	}
	
	/** Method used to scroll view for the element */
	public static void ScrollView(String text ) {

		try {
			MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
					+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"" + text + "\"));");
		
		} catch (Exception e) {
	
		}
	}
	
	
	
	
	/** Method used to click on element using web element */
	public static void clickElement(WebElement element) {
		try {
			if (element != null) {
				element.click();
				
			} else {
				Reporter.log("element is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	}
	
	/** Method used to click on element using xpath */
	public static void clickElement(String xpath) {
		try {
			if (xpath != null) {
				driver.findElement(By.xpath(xpath)).click();
		
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	}
	
	
	/** Method used to send keys to element */
	public void sendText(WebElement element, String text) {
		try {
			if (element != null && text != null) {
				element.click();
				Actions a = new Actions(driver);
				a.sendKeys(text);
				a.perform();
				
			} else {
				Reporter.log("Element or Text is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element or Text is not present: </font><br>" + e);
		}
	}

	/** Method used to send keys to element using xpath */
	public void sendText(String xpath, String text) {
		try {
			if (xpath != null && text != null) {	
				driver.findElement(By.xpath(xpath)).click();
				Actions a = new Actions(driver);
				a.sendKeys(text);
				a.perform();	
				
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element or Text is not present: </font><br>" + e);
		}
	}

	/** Method used to check if element is visible using xpath */
	public boolean elementVisible(String xpath) {
		boolean result = false;
		try {
			if (xpath != null) {
				result = driver.findElement(By.xpath(xpath)).isDisplayed();
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element is not present on page: </font><br>" + e);
		}
		return result;
	}

	/** Method used to check if element is visible using element */
	public boolean elementVisible(WebElement element) {
		boolean result = false;
		try {
			if (element != null) {
				result = element.isDisplayed();
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element is not present on page: </font><br>" + e);
		}
		return result;
	}

	
	/** Method used to hide the keyword */
	public void HideKeyword() {
		
	    try {
	    	driver.pressKey(new KeyEvent(AndroidKey.BACK));
	    } catch (Exception e) {
	        //Lets ignore, apparently its throwing exception when keyboard was not opened
	    }
	}

	
	
	@Step("Verify the assertion cases ")
	public static void ActualResults(WebElement element) throws Exception {
		
		try {
			if (element != null) {
				element.getText();
				
			} else {
				Reporter.log("element is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	
	}
	
	
	
	

	
	
	/** Method used to clear the text */
	public void clearText(String xpath) {
	WebElement toClear = driver.findElement(By.xpath(xpath));
	toClear.sendKeys(Keys.DELETE);
	
	}
	
	
	
	
}









