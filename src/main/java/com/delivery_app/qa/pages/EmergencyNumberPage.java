package com.delivery_app.qa.pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Step;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class EmergencyNumberPage extends FunctionClass {
	AppiumDriver driver;
	String variablename;
	ArrayList<String> data;
	String drawerIcon;
	String emergencyNumber_Link;
	String firstNumber;
	String secondNumber;
	String save_Button;
	String ok_Button;
	String firstNumberText;
	String SecondNumberText;
	

	public void EmergencyNumber() throws InterruptedException {
		try {
			
			setORprop();
			drawerIcon = prop_OR.getProperty("drawerIcon");
			emergencyNumber_Link = prop_OR.getProperty("emergencyNumber_Link");
			firstNumber = prop_OR.getProperty("firstNumber");
			secondNumber = prop_OR.getProperty("secondNumber");
			save_Button = prop_OR.getProperty("save_Button");
			ok_Button = prop_OR.getProperty("ok_Button");
			firstNumberText = prop_OR.getProperty("firstNumberText");
			SecondNumberText = prop_OR.getProperty("SecondNumberText");

			clickElement(drawerIcon);
			clickElement(emergencyNumber_Link);

			// First clear the previous text
			String firstelement = firstNumber;
			
			clickElement(firstNumber);
			clearText(firstelement);
;
			sendText(firstelement, firstNumberText);

			// Second clear the previous text
			String secondelement = secondNumber;
			clickElement(secondNumber);
			clearText(secondelement);

			sendText(secondelement, SecondNumberText);

			HideKeyword();		
			clickElement(save_Button);
			clickElement(ok_Button);

		} catch (Exception e) {

		}
	}
	
}