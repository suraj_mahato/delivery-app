package com.delivery_app.qa.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;

public class OldSearchPage  extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@content-desc=\"Search\"]")
	WebElement search_Xpath;	

	@FindBy(xpath = "//android.widget.EditText[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/search_src_text']")
	WebElement searchingFiled_Xpath;
	
	String radioButton_Xpath="//*[@class ='android.widget.RadioGroup']/android.widget.RadioButton";
	
	
	
	
	
	// Initializing the page objects
		public OldSearchPage() {

			PageFactory.initElements(driver, this);

		}

		// Actions

		/*
		 * click on cash link
		 */

		@Step("click on search icon in home page ")
		public void click_on_Search_link() throws Exception {

	        search_Xpath.click();

		}

		@Step("click randmoly any radio search button ")
		public void select_Randomly_any_searchRadio_Button() throws Exception {

			// Fetch All the time-slot
			List<MobileElement> time_Slot_Count = driver.findElements(By.xpath(radioButton_Xpath));

			// printing the size count of products
			System.out.println("list count:" + time_Slot_Count.size());

			// creating an object of Random class
			Random rnd = new Random();

			// Generates size for random integers products
			int rndInt = rnd.nextInt(time_Slot_Count.size());

			// printing the random size count of products
			System.out.println("random no: " + rndInt);

			// select randomly any products with any index in pending section
			time_Slot_Count.get(rndInt).click();
			
			
		}
		
		@Step("searching any customer")
		public void click_on_SearchFiled_link(String search) throws Exception {

	        searchingFiled_Xpath.click();
	        
	        Thread.sleep(2000);
	        
	        searchingFiled_Xpath.sendKeys(search);

		}
		

}
