package com.delivery_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;

public class PenaltyPage extends  FunctionClass {
	
	
	String drawer_Icon;
	String penaltylink;
	String rejected;
	String accepted;
	String thisMonth;
	String previousMonth;
	String Remark;
	String Submit;
	String pending;

	// Page Factory OR:

	
	@FindBy(xpath="//android.widget.TextView[@text='Rejected']")
	WebElement rejected_Xpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='Accepted']")
	WebElement accepted_Xpath;
	
	@FindBy(xpath="//android.widget.EditText[@text='Enter Remark']")
	WebElement enter_Remark_Xpath;

	@FindBy(xpath="//android.widget.Button[@text='Submit']")
	WebElement submit_Xpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.TextView")
	WebElement pending_Xpath;
	
	

	// Initializing the page objects
	public PenaltyPage() {

		PageFactory.initElements(driver, this);

	}

	// Actions

	/*
	 * click on penality button
	 */

	@Step(" verfiy penatly screen ")
	public void PenaltyScreen() throws Exception {
		
		
		try {
			setORprop();
			drawer_Icon = prop_OR.getProperty("drawer_Icon");
			penaltylink = prop_OR.getProperty("penaltylink");		
			previousMonth = prop_OR.getProperty("previousMonth");
			thisMonth = prop_OR.getProperty("thisMonth");
			clickElement(drawer_Icon);
			clickElement(penaltylink);
			clickElement(previousMonth);
			clickElement(thisMonth);
			

	} catch (Exception e) {

	}
}


	/*
	 * click on rejected button
	 */

	@Step(" click on rejected button")
	public void click_on_Rejected_Button() throws Exception {

		String expected_text = rejected_Xpath.getText();

		Assert.assertEquals("Rejected", expected_text);
				
		//click on rejected button
		rejected_Xpath.click();
	
	
	}
	/*
	 * click on submit button
	 */

	@Step(" click on submit button")
	public void click_on_Submit_Button() throws Exception {
				
		//click on submit button
		submit_Xpath.click();
	
	}	
	

	/*
	 * click on accepted button
	 */

	@Step(" click on accepted button")
	public void click_on_Accepted_Button() throws Exception {


		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"Accepted\"));");
				
		//click on accepted button
		accepted_Xpath.click();
	
	
	}
	

	/*
	 * click on pending button
	 */

	@Step(" click on pending button and enter reamark: {0} ")
	public void click_on_pending_Button(String Enter_Remark) throws Exception {


		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"Pending\"));");
				
		//click on pending button
		pending_Xpath.click();
		
		Thread.sleep(3000);
		
		enter_Remark_Xpath.click();
		Actions a = new Actions(driver);
		a.sendKeys(Enter_Remark);
		a.perform();
		
	
	}
	
	
	

}
