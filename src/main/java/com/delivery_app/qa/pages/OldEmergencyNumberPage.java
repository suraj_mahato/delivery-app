package com.delivery_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;
import io.qameta.allure.Step;

public class OldEmergencyNumberPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement drawerIcon_Xpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Emergency Numbers']")
	WebElement emergencyNumber_Link_Xpath;
	
	@FindBy(xpath="//android.widget.EditText[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/et_first_number']")
	WebElement firstNumber_Filed_Xpath; 
	
	@FindBy(xpath="//android.widget.EditText[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/et_second_number']")
	WebElement secondNumber_Filed_Xpath; 
	
	@FindBy(xpath = "//android.widget.TextView[@text='Save']")
	WebElement save_Button_Xpath;
	
	@FindBy(xpath = "//android.widget.Button[@text='OK']")
	WebElement ok_Button_Xpath;
	
	
	// Initializing the page objects
		public OldEmergencyNumberPage() {

			PageFactory.initElements(driver, this);

		}

		// Actions
		
		/*
		 * click on drawer icon
		 */

		@Step("click on drawer icon ")
		public void click_on_Drawer_Icon() throws Exception {

			// click on drawer icon
			drawerIcon_Xpath.click();
		
		
		}
		
		@Step("click on emergency number link ")
		public void click_on_EmergencyNumber_Link() throws Exception {

			// click on emergency numer link
			emergencyNumber_Link_Xpath.click();
		
		
		}
		
		@Step("Enter first conatct number link ")
		public void enter_First_Contact_Number_Link(String enterFirstText) throws Exception {

			//First clear the previous text
			WebElement firstelement = firstNumber_Filed_Xpath;
			
			firstelement.click();
			
			firstelement.clear();
			
			Thread.sleep(2000);
		
			Actions a = new Actions(driver);
			a.sendKeys(enterFirstText);
			a.perform();	
		
		}
		
		@Step("Enter second conatct number link ")
		public void enter_Second_Contact_Number_Link(String enterSecondText) throws Exception {

			//First clear the previous text
			WebElement secondelement = secondNumber_Filed_Xpath;
			
			secondelement.click();
			
			secondelement.clear();
			
			Thread.sleep(2000);
		
			Actions a = new Actions(driver);
			a.sendKeys(enterSecondText);
			a.perform();	
		
		}
		
		
		@Step("click on save Button ")
		public void click_on_save_Button() throws Exception {

			// click on save Button
			save_Button_Xpath.click();
		
		
		}
		
		@Step("click on ok Button ")
		public void click_on_ok_Button() throws Exception {

			// click on ok Button
			ok_Button_Xpath.click();
		
		
		}
		
		
		
		
		
		

}
