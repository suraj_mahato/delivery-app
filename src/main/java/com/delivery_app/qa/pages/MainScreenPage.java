package com.delivery_app.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.utilities.FunctionClass;

import io.qameta.allure.Step;

public class MainScreenPage extends FunctionClass {
	
	String priority;
	String Pending;
	String Preferred;
	String Done;
	String Cash;
	String Sync;
	String RefreshIcon;
	String search;
	String backIcon;
	
	
	
	// Initializing the page objects
	
	public MainScreenPage() {

		PageFactory.initElements(driver, this);

	}
	

	/*
	 * click on Done button
	 */

	@Step("click one by one by link ")
	public void verifyonebyonelinkMainScreen() throws Exception { 


		try {
			setORprop();
			Done =prop_OR.getProperty("Done");
			priority =prop_OR.getProperty("priority");	
			Preferred =prop_OR.getProperty("Preferred");
			RefreshIcon =prop_OR.getProperty("RefreshIcon");
			Cash =prop_OR.getProperty("Cash");
			Sync =prop_OR.getProperty("Sync");
			Pending =prop_OR.getProperty("Pending");
			search =prop_OR.getProperty("search");
			backIcon =prop_OR.getProperty("backIcon");
			
			
	
			waitUntilFound(priority, 10);
			clickElement(priority);
			
			waitUntilFound(Preferred, 10);
			clickElement(Preferred);
			
			waitUntilFound(Done, 10);
			clickElement(Done);
			
			waitUntilFound(Cash, 10);
			clickElement(Cash);
			
			waitUntilFound(Pending, 10);
			clickElement(Pending);
			
			waitUntilFound(Sync, 10);
			clickElement(Sync);
			
		//	waitUntilFound(RefreshIcon, 10);
		//	clickElement(RefreshIcon);
			
			waitUntilFound(search, 10);
			clickElement(search);
			
			HideKeyword();
			HideKeyword(); 
			
			waitUntilFound(backIcon, 10);
			clickElement(backIcon);
			
			
			}  catch (Exception e) {
				

			}

		}
	

}
