package com.delivery_app.qa.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
public class SearchPage  extends FunctionClass {
	public static WebDriver driver;
	String variablename;
	ArrayList<String> data;
	String search;
	String searchingFiled;
	String radioButton;
	String SearchText;
	
	public void Search() throws InterruptedException {
		try {
			setORprop();
			search = prop_OR.getProperty("search");
			searchingFiled = prop_OR.getProperty("searchingFiled");
			radioButton = prop_OR.getProperty("radioButton");
			SearchText = prop_OR.getProperty("SearchText");
			
			clickElement(search);
			
			
			 Thread.sleep(4000);
			
			// Fetch All the time-slot
			List<WebElement> time_Slot_Count = driver.findElements(By.xpath("//*[@class ='android.widget.RadioGroup']/android.widget.RadioButton"));
			// printing the size count of products
			System.out.println("list count:" + time_Slot_Count.size());
			// creating an object of Random class
			Random rnd = new Random();
			// Generates size for random integers products
			int rndInt = rnd.nextInt(time_Slot_Count.size());
			// printing the random size count of products
			System.out.println("random no: " + rndInt);
			// select randomly any products with any index in pending section
			time_Slot_Count.get(rndInt).click();
			
	
			clickElement(searchingFiled);
	       
	        Thread.sleep(2000);
	       
	        sendText(searchingFiled, SearchText);
	       
//	        test.log(LogStatus.INFO, "Search Successful");
	
	}
		catch (Exception e) {
//			test.log(LogStatus.FAIL, "Search Failed"); 		
		}
	
}
}