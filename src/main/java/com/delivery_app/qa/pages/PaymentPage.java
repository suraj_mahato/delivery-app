package com.delivery_app.qa.pages;

import com.delivery_app.qa.utilities.FunctionClass;
import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.delivery_app.qa.base.TestBase;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Step;

public class PaymentPage extends FunctionClass {
	
	public static WebDriver driver;
	String variablename;
	ArrayList<String> data;
	String Payments;
	String PreviousMonth;
	String ThisMonth;
	String backIcon;
	
	public void Payments() throws InterruptedException {
		try {
			setORprop();
			Payments = prop_OR.getProperty("Payments");
			PreviousMonth = prop_OR.getProperty("PreviousMonth");
			ThisMonth = prop_OR.getProperty("ThisMonth");
			backIcon = prop_OR.getProperty("backIcon");
			
			clickElement(backIcon);
			clickElement(Payments);
			clickElement(PreviousMonth);
			clickElement(ThisMonth);

	//		test.log(LogStatus.INFO, "Payments Successful");
			} catch (Exception e) {
		//		test.log(LogStatus.FAIL, "Payments Failed"); 		
			}
		}
	}
	


