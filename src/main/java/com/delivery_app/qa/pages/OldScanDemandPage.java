package com.delivery_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class OldScanDemandPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@text='Scan Demand']")
	WebElement scanDemand_Xpath;	

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement backIcon_Xpath;
	
	
	
	
	
	// Initializing the page objects
		public OldScanDemandPage() {

			PageFactory.initElements(driver, this);

		}

		// Actions

		/*
		 * click on cash link
		 */

		@Step("click on scan demand link ")
		public void click_on_ScanDemand_link() throws Exception {

			scanDemand_Xpath.click();

		}
		
		@Step("click on back icon link ")
		public void click_on_BackIcon_link() throws Exception {

			backIcon_Xpath.click();
			

		}
		
		

}
