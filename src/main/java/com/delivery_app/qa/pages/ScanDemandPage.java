package com.delivery_app.qa.pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Test;

import io.qameta.allure.Step;

public class ScanDemandPage extends FunctionClass {
	
	public static WebDriver driver;
	String variablename;
	ArrayList<String> data;
	String scanDemand;
	String backIcon;
	
	

	// Initializing the page objects
	
	public ScanDemandPage() {

		PageFactory.initElements(driver, this);

	}
	
	
	public void ScanDemand() throws Exception {
		try {
			setORprop();
			scanDemand = prop_OR.getProperty("scanDemand");
			backIcon = prop_OR.getProperty("backIcon");
			
			clickElement(backIcon);
			clickElement(scanDemand);
			

			
	//		Test.log(LogStatus.INFO, "Scan Demand Successful");
			
			} catch (Exception e) {
				
		//		Test.log(LogStatus.FAIL, "Scan Demand Failed"); 		
			}
		}
	
	
	@Step("Verify the assertion cases ")
	public String ActualResult() throws Exception {
		WebElement text;
		setORprop();
		scanDemand=prop_OR.getProperty("scanDemand");
		
		text=driver.findElement(By.xpath(scanDemand));
		return text.getText();
		
	    
	
	}
	
	}


