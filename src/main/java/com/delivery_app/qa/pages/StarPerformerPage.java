package com.delivery_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;

import io.qameta.allure.Step;

public class StarPerformerPage extends FunctionClass {
	
	String starPerformance;
	String backIcon;


	// Initializing the page objects
	public StarPerformerPage() {

		PageFactory.initElements(driver, this);

	}

	// Actions	
	

	

	public void StarPerformer() throws Exception {
		try {
			setORprop();
			starPerformance = prop_OR.getProperty("starPerformance");
			backIcon = prop_OR.getProperty("backIcon");
			
			clickElement(backIcon);
			clickElement(starPerformance);
	
						
			} catch (Exception e) {
				
			}
		}

	
	
	/*
	
	@Step("click on star performence filed ")
	public void clickOnStarPerformenceFiled() throws Exception {

		// click on star performence filed
		StarPerformerFiledXpath.click();

	}
	
	
	@Step("Click on back Icon")
	public void clickOnBackIcon() throws Exception {
		
		backIconXpath.click();
		
	}
	
	*/
	
	

}
