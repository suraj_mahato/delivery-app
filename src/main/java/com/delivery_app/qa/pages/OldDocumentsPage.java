package com.delivery_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class OldDocumentsPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@text='Documents']")
	WebElement Document_Xpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Pending/Rejected']")
	WebElement pending_Xpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Accepted']")
	WebElement Accepted_Xpath;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement backIcon_Xpath;

	// Initializing the page objects
	public OldDocumentsPage() {

		PageFactory.initElements(driver, this);

	}

	// Actions

	/*
	 * click on cash link
	 */

	@Step("click on document link ")
	public void click_on_Document_link() throws Exception {

		Document_Xpath.click();

	}

	@Step("click on accepting link ")
	public void click_on_Accepting_link() throws Exception {

		Accepted_Xpath.click();

	}

    

	@Step("click on pending/rejected link ")
	public void click_on_Pending_link() throws Exception {

		pending_Xpath.click();

	}
	
	@Step("click on back Icon link ")
	public void click_on_BackIcon_link() throws Exception {

		backIcon_Xpath.click();

	}	

}
