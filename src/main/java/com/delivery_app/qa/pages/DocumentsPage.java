package com.delivery_app.qa.pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Step;

public class DocumentsPage extends FunctionClass {
	public static WebDriver driver;
	String variablename;
	ArrayList<String> data;
	String Document;
	String pending;
	String Accepted;
	String backIcon;

	
	
	
	// Initializing the page objects
	
	public DocumentsPage() {

		PageFactory.initElements(driver, this);

	}
	
	
	public void Documents() throws InterruptedException {
			
		try {
			setORprop();
			Document = prop_OR.getProperty("Document");
			pending = prop_OR.getProperty("pending");
			Accepted = prop_OR.getProperty("Accepted");
			backIcon = prop_OR.getProperty("backIcon");		
			
			clickElement(backIcon);
			clickElement(Document);
			clickElement(Accepted);
			clickElement(pending);
			

	} catch (Exception e) {

	}
}

}

