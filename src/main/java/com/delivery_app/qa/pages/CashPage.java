package com.delivery_app.qa.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;

public class CashPage extends FunctionClass {
	
	// Page Factory OR:
	
	String cash_Link;
	String popup_OpenAppSetting;
	String done_Button;
	String doneWithRemark_Button;
	String NotCollected_Button;
	String cheque_Button;
	String chequeNoFiled;
	String confirmOTPtext;
	String cash_Button;
	String amountPaid_Filed;
	String request;
	String relativeNumber;
	String agentNumber;
	String numberFiled;
	String enterOTPFiled;
	String verifyOTP_Button;
	String billingIssue_Button;
	String donotHaveMC_Button;
	String qualityIssue_Button;
	String deliveryBoy_Button;
	String callCustomer_Button;
	String paid_Button;
	String deliveryBoyCash_Button;
	String deliveryBoyCheque_Button;
	String app_Button;
	String neft_Button;
	String smsLink_Button;
	String willPaytm_Button;
	String willApp_Button;
	String comeLater_Button;
	String houseLocked_Button;
	String other_Button;
	String enterText_Filed;
	String submit_Button;
	String proceed_Button;
	String selectDate_Button;
	String particularDate_Button;
	String ok_Button;
	String SendDateText;
	String enterOtptext;
	String customer_List;
	String EnterOTP;
	String EnterCheque;
	String Number;
	String Amount;
	String text;
	
	String customer_List_Xpath = "//*[@class ='androidx.recyclerview.widget.RecyclerView']/android.widget.LinearLayout";
	
	

	// Initializing the page objects
	public CashPage() {

		PageFactory.initElements(driver, this);

	}

	// Actions
	

	/*
	 * click on cash link
	 */

	@Step("click on cash link ")
	public void click_on_cash_link() throws Exception {
		

		try {
			setORprop();
			cash_Link =prop_OR.getProperty("cash_Link");			
			waitUntilFound(cash_Link, 10);
			clickElement(cash_Link);
	
			
			}  catch (Exception e) {
				

			}

		}
	

	
	
	
	/*
	 * Time slot count Inside time slot how many customer are there
	 */

	@Step("click randmoly any time slot and also select any customer ")
	public void select_Randomly_any_Customer() throws Exception {

		// Fetch All the time-slot
		List<MobileElement> customer_ListCount = driver.findElements(By.xpath(customer_List_Xpath));

		// printing the size count of time-slot
		System.out.println("list count:" + customer_ListCount.size());

		// creating an object of Random class
		Random rnd = new Random();

		// Generates size for random integers time-slot
		int rndInt = rnd.nextInt(customer_ListCount.size());

		
		// printing the random size count of time-slot
		System.out.println("random no: " + rndInt);

		// select randomly any time slot with any index in pending section
		customer_ListCount.get(rndInt).click();
		
		
		
	
}
	
	@Step("click on not collected button ")
	public void click_on_NotCollected_Button() throws Exception {


		try {
			setORprop();
			NotCollected_Button =prop_OR.getProperty("NotCollected_Button");			
			waitUntilFound(NotCollected_Button, 10);
			clickElement(NotCollected_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
		
	
	@Step("click on paid button ")
	public void click_on_Paid_Button() throws Exception {

		
		try {
			setORprop();
			paid_Button =prop_OR.getProperty("paid_Button");			
			waitUntilFound(paid_Button, 10);
			clickElement(paid_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
			

	@Step("click on delivery boy cash button ")
	public void click_on_deliveryBoyCash_Button() throws Exception {

		
		try {
			setORprop();
			deliveryBoyCash_Button =prop_OR.getProperty("deliveryBoyCash_Button");			
			waitUntilFound(deliveryBoyCash_Button, 10);
			clickElement(deliveryBoyCash_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		

	@Step("click on delivery boy cheque button ")
	public void click_on_deliveryBoyCheque_Button() throws Exception {

			try {
				setORprop();
				deliveryBoyCheque_Button =prop_OR.getProperty("deliveryBoyCheque_Button");			
				waitUntilFound(deliveryBoyCheque_Button, 10);
				clickElement(deliveryBoyCheque_Button);
		
				
				}  catch (Exception e) {
					

				}

			}
			
			
	
	@Step("click on App button ")
	public void click_on_App_Button() throws Exception {

		try {
			setORprop();
			app_Button =prop_OR.getProperty("app_Button");			
			waitUntilFound(app_Button, 10);
			clickElement(app_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
	

	
	@Step("click on NEFT button ")
	public void click_on_NEFT_Button() throws Exception {

		try {
			setORprop();
			neft_Button =prop_OR.getProperty("neft_Button");			
			waitUntilFound(neft_Button, 10);
			clickElement(neft_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
	
	
	@Step("click on sms Link button ")
	public void click_on_SMSLink_Button() throws Exception {


		try {
			setORprop();
			smsLink_Button =prop_OR.getProperty("smsLink_Button");			
			waitUntilFound(smsLink_Button, 10);
			clickElement(smsLink_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		


	@Step("click on will pay by paytam button")
	public void click_on_WillPayByPaytm_Button() throws Exception {


		try {
			setORprop();
			willPaytm_Button =prop_OR.getProperty("willPaytm_Button");	
			selectDate_Button =prop_OR.getProperty("selectDate_Button");
			particularDate_Button =prop_OR.getProperty("particularDate_Button");
			ok_Button =prop_OR.getProperty("ok_Button");
			
			waitUntilFound(willPaytm_Button, 10);
			clickElement(willPaytm_Button);
			
			waitUntilFound(selectDate_Button, 10);
			clickElement(selectDate_Button);
			
			waitUntilFound(particularDate_Button, 10);
			clickElement(particularDate_Button);
			
			waitUntilFound(ok_Button, 10);
			clickElement(ok_Button);
			
			
	
			
			}  catch (Exception e) {
				

			}

		}
		
		

	@Step("click on will pay by App button ")
	public void click_on_WillPayApp_Button() throws Exception {

		

		try {
			setORprop();
			willApp_Button =prop_OR.getProperty("willApp_Button");	
			selectDate_Button =prop_OR.getProperty("selectDate_Button");
			particularDate_Button =prop_OR.getProperty("particularDate_Button");
			ok_Button =prop_OR.getProperty("ok_Button");
			
			waitUntilFound(willApp_Button, 10);
			clickElement(willApp_Button);
			
			waitUntilFound(selectDate_Button, 10);
			clickElement(selectDate_Button);
			
			waitUntilFound(particularDate_Button, 10);
			clickElement(particularDate_Button);
			
			waitUntilFound(ok_Button, 10);
			clickElement(ok_Button);
			
			
	
			
			}  catch (Exception e) {
				

			}

		}
		
		

	
	@Step("click on come Later button")
	public void click_on_ComeLater_Button() throws Exception {


		try {
			setORprop();
			comeLater_Button =prop_OR.getProperty("comeLater_Button");	
			selectDate_Button =prop_OR.getProperty("selectDate_Button");
			particularDate_Button =prop_OR.getProperty("particularDate_Button");
			ok_Button =prop_OR.getProperty("ok_Button");
			
			waitUntilFound(comeLater_Button, 10);
			clickElement(comeLater_Button);
			
			waitUntilFound(selectDate_Button, 10);
			clickElement(selectDate_Button);
			
			waitUntilFound(particularDate_Button, 10);
			clickElement(particularDate_Button);
			
			waitUntilFound(ok_Button, 10);
			clickElement(ok_Button);
			
			
	
			
			}  catch (Exception e) {
				

			}

		}
		
		

	
	@Step("click on house Locked button")
	public void click_on_HouseLocked_Button() throws Exception {


		try {
			setORprop();
			houseLocked_Button =prop_OR.getProperty("houseLocked_Button");			
			waitUntilFound(houseLocked_Button, 10);
			clickElement(houseLocked_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		

	@Step("click on billing issue button")
	public void click_on_billingIssue_Button() throws Exception {

		try {
			setORprop();
			billingIssue_Button =prop_OR.getProperty("billingIssue_Button");	
			ScrollView("Billing Issue");
			waitUntilFound(billingIssue_Button, 10);
			clickElement(billingIssue_Button);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	

	
	@Step("click on other button")
	public void click_on_other_Button() throws Exception {

		try {
			setORprop();
			other_Button =prop_OR.getProperty("other_Button");	
			enterText_Filed=prop_OR.getProperty("enterText_Filed");
			text =prop_OR.getProperty("text");
			ScrollView("Other");
			waitUntilFound(other_Button, 10);
			clickElement(other_Button);
	        sendText(enterText_Filed, text);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	

	
	@Step("click on submit button")
	public void click_on_submit_Button() throws Exception {


		try {
			setORprop();
			submit_Button =prop_OR.getProperty("submit_Button");			
			waitUntilFound(submit_Button, 10);
			clickElement(submit_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		

	@Step("click on popup message i.e Open app setting button")
	public void click_on_OpenAppSetting_Button() throws Exception {

		try {
			setORprop();
			popup_OpenAppSetting =prop_OR.getProperty("popup_OpenAppSetting");			
			waitUntilFound(popup_OpenAppSetting, 10);
			clickElement(popup_OpenAppSetting);
	
			
			}  catch (Exception e) {
				

			}

		}
		

	
	@Step("click on proceed button")
	public void click_on_proceed_Button() throws Exception {

		try {
			setORprop();
			proceed_Button =prop_OR.getProperty("proceed_Button");			
			waitUntilFound(proceed_Button, 10);
			clickElement(proceed_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
			
		
		
	
	@Step("click on Done with Remark button")
	public void click_on_DoneWithRemark_Button() throws Exception {
	

		try {
			setORprop();
			doneWithRemark_Button =prop_OR.getProperty("doneWithRemark_Button");			
			waitUntilFound(doneWithRemark_Button, 10);
			clickElement(doneWithRemark_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
		
	
	
	@Step("click on cash button")
	public void click_on_cash_Button() throws Exception {


		try {
			setORprop();
			cash_Button =prop_OR.getProperty("cash_Button");			
			waitUntilFound(cash_Button, 10);
			clickElement(cash_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
	
	@Step("click on cheque button")
	public void click_on_cheque_Button() throws Exception {


		try {
			setORprop();
			cheque_Button =prop_OR.getProperty("cheque_Button");			
			waitUntilFound(cheque_Button, 10);
			clickElement(cheque_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		

	
	@Step("Enter Amount")
	public void enterAmount() throws Exception {
		
		try {
			setORprop();
			amountPaid_Filed = prop_OR.getProperty("amountPaid_Filed");
			Amount = prop_OR.getProperty("Amount");
			
			waitUntilFound(numberFiled, 10);
			sendText(amountPaid_Filed, Amount);
			
		} catch (Exception e) {

			
		}

	}

	
	@Step("click on Billing button")
	public void click_on_BillingIssue_Button() throws Exception {


		try {
			setORprop();
			billingIssue_Button =prop_OR.getProperty("billingIssue_Button");			
			waitUntilFound(billingIssue_Button, 10);
			clickElement(billingIssue_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
	
	@Step("click on Do not have money change button")
	public void click_on_DonotHaveMoneyChange_Button() throws Exception {

		try {
			setORprop();
			donotHaveMC_Button =prop_OR.getProperty("donotHaveMC_Button");			
			waitUntilFound(donotHaveMC_Button, 10);
			clickElement(donotHaveMC_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
	
	@Step("click on Quality Issue button")
	public void click_on_QualityIssue_Button() throws Exception {
		
		try {
			setORprop();
			qualityIssue_Button =prop_OR.getProperty("qualityIssue_Button");			
			waitUntilFound(qualityIssue_Button, 10);
			clickElement(qualityIssue_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		

	
	@Step("click on Delivery Boy button")
	public void click_on_DeliveryBoy_Button() throws Exception {


		try {
			setORprop();
			deliveryBoy_Button =prop_OR.getProperty("deliveryBoy_Button");			
			waitUntilFound(deliveryBoy_Button, 10);
			clickElement(deliveryBoy_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		

	
	@Step("click on Delivery Boy button")
	public void click_on_CallToCustomer_Button() throws Exception {


		try {
			setORprop();
			callCustomer_Button =prop_OR.getProperty("callCustomer_Button");			
			waitUntilFound(callCustomer_Button, 10);
			clickElement(callCustomer_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
	
	
	@Step("click on request button")
	public void click_on_Request_Button() throws Exception {


		try {
			setORprop();
			request =prop_OR.getProperty("request");			
			waitUntilFound(request, 10);
			clickElement(request);
	
			
			}  catch (Exception e) {
				

			}

		}
		
		
	
	
	@Step("Enter otp number")
	public void enterOTP() throws Exception {


		try {
			setORprop();
			enterOTPFiled = prop_OR.getProperty("enterOTPFiled");
			EnterOTP = prop_OR.getProperty("EnterOTP");
			
			waitUntilFound(EnterOTP, 10);
			sendText(enterOTPFiled, EnterOTP);
			
		} catch (Exception e) {

			
		}

	}
		
		

	
	@Step("Enter cheque number")
	public void enterChequeNumber() throws Exception {

		
		try {
			setORprop();
			chequeNoFiled = prop_OR.getProperty("chequeNoFiled");
			EnterCheque = prop_OR.getProperty("EnterCheque");
		
			waitUntilFound(chequeNoFiled, 10);
			sendText(chequeNoFiled, EnterCheque);
			
		} catch (Exception e) {

			
		}

	}
		
		

	
	@Step("click on verify OTP button")
	public void click_on_VerifyOTP_Button() throws Exception {


		try {
			setORprop();
			verifyOTP_Button =prop_OR.getProperty("verifyOTP_Button");			
			waitUntilFound(verifyOTP_Button, 10);
			clickElement(verifyOTP_Button);
	
			
			}  catch (Exception e) {
				

			}

		}
				

	@Step("click on relative number radio button")
	public void click_on_RelativeNumber_Button() throws Exception {
	

		try {
			setORprop();
			relativeNumber =prop_OR.getProperty("relativeNumber");			
			waitUntilFound(relativeNumber, 10);
			clickElement(relativeNumber);
	
			
			}  catch (Exception e) {
				

			}

		}
		


	@Step("click on Agent number radio button")
	public void click_on_AgentNumber_Button() throws Exception {
		

		try {
			setORprop();
			agentNumber =prop_OR.getProperty("agentNumber");			
			waitUntilFound(agentNumber, 10);
			clickElement(agentNumber);
	
			
			}  catch (Exception e) {
				

			}

		}	
		

	@Step("Enter mobile number")
	public void enterMobileNumber() throws Exception {

		try {
			setORprop();
			numberFiled = prop_OR.getProperty("numberFiled");
			Number = prop_OR.getProperty("Number");
			
			waitUntilFound(numberFiled, 10);
			sendText(numberFiled, Number);
			
		} catch (Exception e) {

			
		}

	}
}
	
		
		
	
	
	



