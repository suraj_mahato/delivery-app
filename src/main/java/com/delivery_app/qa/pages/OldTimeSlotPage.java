package com.delivery_app.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.delivery_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class OldTimeSlotPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/img_select_slot']")
	WebElement TimeSlotIconXpath;
	
	
	

	
	// Initializing the page objects
		public OldTimeSlotPage() {

			PageFactory.initElements(driver, this);

		}

		// Actions

		/*
		 * click on cash link
		 */

		@Step("click on timeslot icon on home screen ")
		public void clickOnTimeSlotIconlink() throws Exception {
			
			//click on timeslot icon
			TimeSlotIconXpath.click();
			

		}
		
		
		@Step("Select one by one time slot button")
		public void SelectOneByOneTimeSlotButton() throws Exception {
			
			
			// finding list of timeslot filed
			List<WebElement> elements = driver.findElements(By.xpath("//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout"));
			
			System.out.println("list count:" + elements.size());

			for (int i = 0; i < elements.size(); i++) {
				
				
				// click on timeslot text by one by one
				elements.get(i).click();
				Thread.sleep(2000);
		
				System.out.println("Index of i is :-" + Integer.toString(i));
	
				
				TimeSlotIconXpath.click();
				Thread.sleep(2000);
				
				String titlexpath="//android.widget.TextView[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/tv_time_slot']";
				
				
				WebElement nameText = driver.findElement(By.xpath(titlexpath));

				// printing title with description of FAQ 
				System.out.println("Timeslot Selected Text : " + nameText.getText().toString());

		
				
				
			
			
		   }

			
		}
		
		
	
}
