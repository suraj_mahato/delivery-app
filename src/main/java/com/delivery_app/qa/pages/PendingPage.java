package com.delivery_app.qa.pages;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
import com.relevantcodes.extentreports.LogStatus;

public class PendingPage extends FunctionClass {


	String FutureQtyChange;
    String NotDelivered;
	String milkLeakage;
	String Remark;
	String text;
	String ProductShortage;
	String RouteLate;
	String OutOfDeliveryArea;
	String WrongRoute;
	String CustomerSide;
	String OurSide;
	String Hold;
	String HoldTodayOnly;
	String HoldTodayandChangeQuanity;
	String HoldFurther;
	String Stop;
	String Quality;
	String Service;
	String GoingOutofStation;
	String Billing;
	String Timing;
	String Others;
	String OnlyForTrail;
	String NotRequired;
	String HouseLocked;
	String PhoneUnreachable;
	String QualityIssue;
	String BillingIssue;
	String MutipleIds;
	String AddressIssue;
	String Pending;
	String RemarkDone;
	String Done;
	String DonewithRemark;
	String Continue;
	String FutureHold;
	String FutureStop;
	String CallToCustomer;
	String Other;
	String Yes;
	String No;
	String AlternateDays;
	String Ok;
	String ParticularDate;
	String Optional;
	String MNDContinue;
	

	// Page Factory OR:


	
	@FindBy(xpath="//android.widget.Button[@resource-id='android:id/button1']")
	WebElement popupOkXpath;
	
	@FindBy(xpath="//android.widget.EditText[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/etCustOtp']")
	WebElement EnterOtpTextBox;
	
	@FindBy(xpath="//android.widget.Button[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/btnSubmit']")
	WebElement SubmitButton;
	
	@FindBy(xpath="//android.widget.Button[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/btnCancel']")
	WebElement CancelButton;
	
	String popupOKXpath="//android.widget.Button[@resource-id='android:id/button1'";
	
	String radioButton_Xpath="//*[@class ='androidx.recyclerview.widget.RecyclerView']/android.view.ViewGroup";
	
	String time_slot_Xpath = "//*[@class ='android.widget.ExpandableListView']/android.widget.RelativeLayout";

	String inside_slot_Xpath = "//*[@class ='android.widget.ExpandableListView']/android.widget.RelativeLayout/android.widget.FrameLayout";

	
	// Initializing the page objects
	
	public PendingPage() {

		PageFactory.initElements(driver, this);

	}
	
	

	// Actions

	/*
	 * Time slot count Inside time slot how many customer are there
	 */

	@Step("click randmoly any time slot and also select any customer ")
	public void select_Randomly_any_Slot() throws Exception {

		// Fetch All the time-slot
		List<MobileElement> time_Slot_Count = driver.findElements(By.xpath(time_slot_Xpath));

		// printing the size count of time-slot
		System.out.println("list count:" + time_Slot_Count.size());

		// creating an object of Random class
		Random rnd = new Random();

		// Generates size for random integers time-slot
		int rndInt = rnd.nextInt(time_Slot_Count.size());

		// printing the random size count of time-slot
		System.out.println("random no: " + rndInt);

		// select randomly any time slot with any index in pending section
		time_Slot_Count.get(rndInt).click();

		// Fetch All the time-slot inside customer count
		List<MobileElement> time_Slot_Count_inside_Count = driver.findElements(By.xpath(inside_slot_Xpath));

		// printing the size of customer count
		System.out.println("list count inside:" + time_Slot_Count_inside_Count.size());

		// creating an object of Random class
		Random rnd1 = new Random();

		// Generates size for random integers for customer
		int rndInt1 = rnd1.nextInt(time_Slot_Count_inside_Count.size());

		// printing the size of random customer
		System.out.println("random no inside: " + rndInt1);

		// select randomly any time slot with any index in pending section
		time_Slot_Count_inside_Count.get(rndInt1).click();
		
	//	time_Slot_Count_inside_Count.get(rndInt1).click();

		//logic is for:- when popup is display or not
		
		System.out.println("Time Check: "+new Date());
 	
		if(!(driver.findElements(By.xpath("//android.widget.Button[@resource-id='android:id/button1']")).isEmpty())) {
			
			//click on ok button
			System.out.println("Popup display");
	     	popupOkXpath.click();
	     	
	     	
			Thread.sleep(3000);
			time_Slot_Count_inside_Count.get(rndInt1).click();

			
			
			
		}else {
				
			
			System.out.println("Popup not display");
			
        	time_Slot_Count_inside_Count.get(rndInt1).click();		
			
		}
		

		
	}
	
	
	/*
	 * click on Done button
	 */

	@Step("click on Done button ")
	public void click_on_Done_Button() throws Exception { 


		try {
			setORprop();
			Done =prop_OR.getProperty("Done");			
			ScrollView("Done");
			waitUntilFound(Done, 10);
			clickElement(Done);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	
	

	/*
	 * click on blue Done button
	 */

	@Step("click on blue Done button ")
	public void blue_Done_Button() throws Exception {

		try {
			setORprop();
			RemarkDone =prop_OR.getProperty("RemarkDone");			
			ScrollView("Done");
			waitUntilFound(RemarkDone, 10);
			clickElement(RemarkDone);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
		
		

	
	/*
	 * click on Done with remark button
	 */

	@Step("click on Done with Remark button ")
	public void click_on_Done_with_Remark_Button() throws Exception {

		try {
			setORprop();
			DonewithRemark =prop_OR.getProperty("DonewithRemark");			
			waitUntilFound(DonewithRemark, 10);
			clickElement(DonewithRemark);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	
	
	
	/*
	 * click on Continue button
	 */
	
	@Step("click on continue button")
	public void click_on_Continue_Button() throws Exception {


		try {
			setORprop();
			Continue =prop_OR.getProperty("Continue");	
			ScrollView("Continue");
			waitUntilFound(Continue, 10);
			clickElement(Continue);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	
	
	/*
	 * Select first done with remark, select Future qty change button
	 */

	@Step("Select first done with remark, select Future qty change button ")
	public void Future_QtyChange_Button() throws Exception {

		try {
			setORprop();
			FutureQtyChange=prop_OR.getProperty("FutureQtyChange");
			waitUntilFound(FutureQtyChange, 10);
			clickElement(FutureQtyChange);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}

	
	
	/*
	 * click on future Hold button
	 */

	@Step("click on future hold button ")
	public void click_on_Future_Hold_Button() throws Exception {

		try {
			setORprop();
			FutureHold=prop_OR.getProperty("FutureHold");
			waitUntilFound(FutureHold, 10);
			clickElement(FutureHold);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	

	/*
	 * click on future stop button
	 */

	@Step("click on future stop button ")
	public void click_on_Future_Stop_Button() throws Exception {

		try {
			setORprop();
			FutureStop=prop_OR.getProperty("FutureStop");
			waitUntilFound(FutureStop, 10);
			clickElement(FutureStop);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	

	/*
	 * click on Quality Issue button
	 */

	@Step("click on Quality Issue button ")
	public void click_on_Quality_Issue_Button() throws Exception {

		
		try {
			setORprop();
			QualityIssue =prop_OR.getProperty("QualityIssue");
			waitUntilFound(QualityIssue, 10);
			clickElement(QualityIssue);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}

	
	

	/*
	 * click on Billing issue button
	 */

	@Step("click on Billing issue button ")
	public void click_on_Billing_Issue_Button() throws Exception {

		try {
			setORprop();
			BillingIssue =prop_OR.getProperty("BillingIssue");			
			ScrollView("Billing Issue");
			waitUntilFound(BillingIssue, 10);
			clickElement(BillingIssue);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		

	/*
	 * click on call to customer button
	 */

	@Step("click on call to customer button ")
	public void click_on_call_to_Customer_Button() throws Exception {

		try {
			setORprop();
			CallToCustomer =prop_OR.getProperty("CallToCustomer");			
			ScrollView("Call to Customer");
			waitUntilFound(CallToCustomer, 10);
			clickElement(CallToCustomer);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	/*
	 * click on remark section and enter any remark text
	 */

	@Step("click on remark section and enter any remark text ")
	public void click_on_Remark_Section_Enter_Text() throws Exception {

		try {
			setORprop();
			Remark = prop_OR.getProperty("Remark");
			text =prop_OR.getProperty("text");
			waitUntilFound(Remark, 10);
			sendText(Remark, text);
			
		} catch (Exception e) {

		}

	}
	

	
	/*
	 * click on other button
	 */

	@Step("click on other button ")
	public void click_on_Other_Button(String type_Here) throws Exception {	

		

		try {
			setORprop();
			Other = prop_OR.getProperty("Other");
			Remark = prop_OR.getProperty("Remark");
			text =prop_OR.getProperty("text");
			ScrollView("Other");
			waitUntilFound(Other, 10);
			clickElement(Other);
			waitUntilFound(Remark, 10);
			sendText(Remark, text);
			
		} catch (Exception e) {

		}

	}
		
		

	/*
	 * click on alternate days radio button
	 */

	@Step("click on alternate days radio button ")
	public void click_Radio_Button() throws Exception {
		
		try {
			setORprop();
			AlternateDays =prop_OR.getProperty("AlternateDays");			
			ScrollView("Alternate Days");
			waitUntilFound(AlternateDays, 10);
			clickElement(AlternateDays);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}	
		

	/*
	 * click on End date right arrow and select any particular date
	 */

	@Step("click on End date right arrow and select any particular date")
	public void click_Duration_Button() throws Exception {

		try {
			setORprop();
			Optional =prop_OR.getProperty("Optional");	
			Ok =prop_OR.getProperty("Ok");
			ParticularDate =prop_OR.getProperty("ParticularDate");	
			ScrollView("(Optional)");
			waitUntilFound(Optional, 10);
			clickElement(Optional);
			waitUntilFound(Ok, 10);
			clickElement(Ok);
			waitUntilFound(ParticularDate, 10);
			clickElement(ParticularDate);
			waitUntilFound(Ok, 10);
			clickElement(Ok);
				
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		

	
	
	/*
	 * click on Not Delivered button
	 */
	
	@Step(" click on Not Delivered button")
	public void click_on_Not_Delivered_Button() throws Exception {
		
		try {
			setORprop();
			NotDelivered=prop_OR.getProperty("NotDelivered");
			waitUntilFound(NotDelivered, 10);
			clickElement(NotDelivered);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}

	
	/*
	 * click on Customer side button
	 */
	
	@Step(" click on Customer side button")
	public void click_on_Customer_Side_Button() throws Exception {
		
		try {
		setORprop();
		CustomerSide=prop_OR.getProperty("CustomerSide");
		waitUntilFound(CustomerSide, 10);
		clickElement(CustomerSide);
		
//		 test.log(LogStatus.INFO, "milk leakage Successful");

		
		}  catch (Exception e) {
			
//			 test.log(LogStatus.FAIL, "milk leakage Failed");

		}

	}

	
	
	/*
	 * click on our side button
	 */
	
	@Step("click on our side button")
	public void click_on_Our_Side_Button() throws Exception {
		
		
		try {
			setORprop();
			OurSide=prop_OR.getProperty("OurSide");
			waitUntilFound(OurSide, 10);
			clickElement(OurSide);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	
	/*
	 * click on Hold button
	 */
	
	@Step(" click on Hold button")
	public void click_on_Hold_Button() throws Exception { 

		
		
		try {
			setORprop();
			Hold=prop_OR.getProperty("Hold");
			waitUntilFound(Hold, 10);
			clickElement(Hold);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	/*
	 * click on Hold Today only button
	 */
	
	@Step("click on Hold Today only button")
	public void click_on_Hold_Today_Only_Button() throws Exception {
	
		
		
		try {
			setORprop();
			HoldTodayOnly=prop_OR.getProperty("HoldTodayOnly");
			waitUntilFound(HoldTodayOnly, 10);
			clickElement(HoldTodayOnly);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
		

	/*
	 * click on Hold Today and change quantity button
	 */
	
	@Step("click on Hold Today and change quantity button")
	public void click_on_Hold_Today_And_Change_Quantity_Button() throws Exception {

		
		try {
			setORprop();
			HoldTodayandChangeQuanity=prop_OR.getProperty("HoldTodayandChangeQuanity");
			waitUntilFound(HoldTodayandChangeQuanity, 10);
			clickElement(HoldTodayandChangeQuanity);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
		

	
	/*
	 * click on Hold further button
	 */
	
	@Step("click on Hold further button")
	public void click_on_Hold_Further_Button() throws Exception {
		
		try {
			setORprop();
			HoldFurther=prop_OR.getProperty("HoldFurther");
			waitUntilFound(HoldFurther, 10);
			clickElement(HoldFurther);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	

	/*
	 * click on stop button
	 */
	
	@Step("click on stop button")
	public void click_on_stop_Button() throws Exception {

		try {
			setORprop();
			Stop=prop_OR.getProperty("Stop");
			waitUntilFound(Stop, 10);
			clickElement(Stop);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	/*
	 * click on First stop button then click on Quality button
	 */
	
	@Step("click on First stop button then click on Quality button")
	public void click_on_stop_QualityButton() throws Exception {

		try {
			setORprop();
			Quality =prop_OR.getProperty("Quality");
			waitUntilFound(Quality, 10);
			clickElement(Quality);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	
	/*
	 * click on First stop button then click on service button
	 */
	
	@Step("click on First stop button then click on service button")
	public void click_on_stop_ServiceButton() throws Exception {

		try {
			setORprop();
			Service =prop_OR.getProperty("Service");
			waitUntilFound(Service, 10);
			clickElement(Service);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
		
	
	/*
	 * click on First stop button then click on Going out of station button
	 */
	
	@Step("click on First stop button then click on Going out of station button")
	public void click_on_stop_GoingOutOfStationButton() throws Exception {

		
		try {
			setORprop();
			GoingOutofStation =prop_OR.getProperty("GoingOutofStation");
			waitUntilFound(GoingOutofStation, 10);
			clickElement(GoingOutofStation);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
		
	
	/*
	 * click on First stop button then click on Billing button
	 */
	
	@Step("click on First stop button then click on billing button")
	public void click_on_stop_BillingButton() throws Exception {

		
		try {
			setORprop();
			Billing =prop_OR.getProperty("Billing");
			waitUntilFound(Billing, 10);
			clickElement(Billing);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
		
		

	
	/*
	 * click on First stop button then click on Timing button
	 */
	
	@Step("click on First stop button then click on Timing button")
	public void click_on_stop_TimingButton() throws Exception {

		try {
			setORprop();
			Timing =prop_OR.getProperty("Timing");
			waitUntilFound(Timing, 10);
			clickElement(Timing);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	
	/*
	 * click on First stop button then click on others button
	 */
	
	@Step("click on First stop button then click on others button")
	public void click_on_stop_OthersButton() throws Exception {


		try {
			setORprop();
			Others =prop_OR.getProperty("Others");			
			ScrollView("Others");
			waitUntilFound(Others, 10);
			clickElement(Others);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	
	
	/*
	 * click on First stop button then click on Only for trails button
	 */
	
	@Step("click on First stop button then click on Only for trails button")
	public void click_on_stop_OnlyForTrailButton() throws Exception {
		

		try {
			setORprop();
			OnlyForTrail =prop_OR.getProperty("OnlyForTrail");			
			ScrollView("Only For Trial");
			waitUntilFound(OnlyForTrail, 10);
			clickElement(OnlyForTrail);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
		
	
	
	/*
	 * click on First stop button then click on Not Required button
	 */
	
	@Step("click on First stop button then click on Not Required button")
	public void click_on_NotRequiredButton() throws Exception {
		

		try {
			setORprop();
			NotRequired =prop_OR.getProperty("NotRequired");			
			ScrollView("Not Required");
			waitUntilFound(NotRequired, 10);
			clickElement(NotRequired);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	
	/*
	 * click on First stop button then click on Out of delivery area button
	 */
	
	@Step("click on First stop button then click on Out of Delivery Area button")
	public void click_on_OutOfDeliveryAreaButton() throws Exception {


		try {
			setORprop();
			OutOfDeliveryArea =prop_OR.getProperty("OutOfDeliveryArea");			
			ScrollView("Out of Delivery Area");
			waitUntilFound(OutOfDeliveryArea, 10);
			clickElement(OutOfDeliveryArea);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	


	/*
	 * click on house locked button
	 */
	
	@Step("click on house locked button")
	public void click_on_House_Locked_Button() throws Exception {
	
		try {
			setORprop();
			HouseLocked =prop_OR.getProperty("HouseLocked");
			waitUntilFound(HouseLocked, 10);
			clickElement(HouseLocked);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//				 test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	
	
	@Step("click on Phone Unreachabled button")
	public void click_on_Phone_Unreachable_Button() throws Exception {

		try {
			setORprop();
			PhoneUnreachable =prop_OR.getProperty("PhoneUnreachable");
			waitUntilFound(PhoneUnreachable, 10);
			clickElement(PhoneUnreachable);

			
			}  catch (Exception e) {
				

				System.out.println("PhoneUnreachable Failed ");
				
			}

		}


	

	/*
	 * click on multiple Ids button
	 */
	
	@Step("click on multiple Ids button")
	public void click_on_Mutiple_Ids_Button() throws Exception {

		try {
			setORprop();
			MutipleIds =prop_OR.getProperty("MutipleIds");			
			ScrollView("Multiple IDs");
			waitUntilFound(MutipleIds, 10);
			clickElement(MutipleIds);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	
	
	/*
	 * click on address issue button
	 */
	
	@Step("click on address issue button")
	public void click_on_Address_Issue_Button() throws Exception {

		try {
			setORprop();
			AddressIssue =prop_OR.getProperty("AddressIssue");			
			ScrollView("Address Issue");
			waitUntilFound(AddressIssue, 10);
			clickElement(AddressIssue);
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}
	


	/*
	 * click on ok and contiue button
	 */
	
	@Step("click on ok and contiue button")
	public void click_on_Ok_Continue_Button() throws Exception {
	
		
		try {
			setORprop();
			Ok =prop_OR.getProperty("Ok");
			MNDContinue=prop_OR.getProperty("MNDContinue");
			
			waitUntilFound(Ok, 10);
			clickElement(Ok);
			
			waitUntilFound(MNDContinue, 10);
			clickElement(MNDContinue);
			
			
			
//			 test.log(LogStatus.INFO, "milk leakage Successful");

			
			}  catch (Exception e) {
				
//		   test.log(LogStatus.FAIL, "milk leakage Failed");

			}

		}	
	
	
	
	/*
	 * click on milk leakage button
	 */

	@Step(" click on milk leakage button")
	public void clickonMilkLeakgeButton() throws InterruptedException {
		
		try {
		setORprop();
		milkLeakage=prop_OR.getProperty("milkLeakage");
		waitUntilFound(milkLeakage, 10);	
		clickElement(milkLeakage);
		
//		 test.log(LogStatus.INFO, "milk leakage Successful");

		
		}  catch (Exception e) {
			
			
			
	//		 test.log(LogStatus.FAIL, "milk leakage Failed");

		}
	
	}


	/*
	 * click on Product shortage button
	 */
	

	@Step(" click on Product shortage button")
	public void clickonProductShortageButton() throws Exception {

		
		try {
			setORprop();
			ProductShortage=prop_OR.getProperty("ProductShortage");
			waitUntilFound(ProductShortage, 10);
			clickElement(ProductShortage);
			
		//	 test.log(LogStatus.INFO, "Product shortage Successful");

				
		}  catch (Exception e) {
			
		//	 test.log(LogStatus.FAIL, "Product shortage Failed");

		}
	
	}
	
	
	/*
	 * click on Route late button
	 */

	@Step(" click on Route late button")
	public void clickonRouteLate_Button() throws Exception {

		try {
			setORprop();
			RouteLate=prop_OR.getProperty("RouteLate");
			waitUntilFound(RouteLate, 10);
			clickElement(RouteLate);
			
		//	 test.log(LogStatus.INFO, "Product shortage Successful");

				
		}  catch (Exception e) {
			
		//	 test.log(LogStatus.FAIL, "Product shortage Failed");

		}
	
	}
	

	/*
	 * click on Out of delivery area button
	 */

	@Step(" click on Out of delivery area button")
	public void clickonOutOfDeliveryAreaButton() throws Exception {

		try {
			setORprop();
			OutOfDeliveryArea=prop_OR.getProperty("OutOfDeliveryArea");
			waitUntilFound(OutOfDeliveryArea, 10);
			clickElement(OutOfDeliveryArea);
			
		//	 test.log(LogStatus.INFO, "Product shortage Successful");

				
		}  catch (Exception e) {
			
		//	 test.log(LogStatus.FAIL, "Product shortage Failed");

		}
	
	}
	
	
	
	/*
	 * click on Wrong Route button
	 */

	@Step(" click on Wrong Route button")
	public void clickonWrongRouteButton() throws Exception {


		try {
			setORprop();
			WrongRoute=prop_OR.getProperty("WrongRoute");
			waitUntilFound(WrongRoute, 10);
			clickElement(WrongRoute);
			
		//	 test.log(LogStatus.INFO, "Product shortage Successful");

				
		}  catch (Exception e) {
			
		//	 test.log(LogStatus.FAIL, "Product shortage Failed");

		}
	
	}
	
	
	/*
	 * click on click on popup yes button
	 */

	@Step(" click on popup yes button")
	public void click_on_Popup_Yes_Button() throws Exception {


		try {
			setORprop();
			Yes=prop_OR.getProperty("Yes");
			waitUntilFound(Yes, 10);
			clickElement(Yes);
			
		//	 test.log(LogStatus.INFO, "Product shortage Successful");

				
		}  catch (Exception e) {
			
		//	 test.log(LogStatus.FAIL, "Product shortage Failed");

		}
	
	}
	
	

	/*
	 * click on click on popup No button
	 */

	@Step(" click on popup No button")
	public void click_on_Popup_No_Button() throws Exception {

		try {
			setORprop();
			No=prop_OR.getProperty("No");
			waitUntilFound(No, 10);
			clickElement(No);
			
		//	 test.log(LogStatus.INFO, "Product shortage Successful");

				
		}  catch (Exception e) {
			
		//	 test.log(LogStatus.FAIL, "Product shortage Failed");

		}
	
	}
	
	/*
	 * click randmoly any radio button of products 
	 */
	
	@Step("click randmoly any radio button of products ")
	public void selectRandomllyAnyRadioButtonOfProducts() throws Exception {

		// Fetch All the time-slot
		List<MobileElement> time_Slot_Count = driver.findElements(By.xpath(radioButton_Xpath));

		// printing the size count of products
		System.out.println("list count:" + time_Slot_Count.size());

		// creating an object of Random class
		Random rnd = new Random();

		// Generates size for random integers products
		int rndInt = rnd.nextInt(time_Slot_Count.size());

		// printing the random size count of products
		System.out.println("random no: " + rndInt);

		// select randomly any products with any index in pending section
		time_Slot_Count.get(rndInt).click();
		
		
	}
	
	
	/*
	 * Enter the OTP First then click on Submit button 
	 */
	
	
	@Step("Enter the OTP First then click on Submit button ")
	public void EnterOTPandSubmit(String EnterOtp) throws Exception {
		

		if(!(driver.findElements(By.xpath("//android.widget.TextView[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/alertTitle']")).isEmpty())) {
			
             
			//click on Enter OTP textbox
			EnterOtpTextBox.click();

			Actions a = new Actions(driver);
			a.sendKeys(EnterOtp);
			a.perform();
				

			// explicit wait - to wait for the submit button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/btnSubmit']")));

			// click on the submit button as soon as the submit button is visible
			SubmitButton.click();
			
			
		}else {
				
			
			System.out.println("OTP Popup not display");

			
		}
		
	}
	
	
	@Step("Verify the assertion cases ")
	public String ActualResult() throws Exception {
		WebElement text;
		setORprop();
		Pending=prop_OR.getProperty("Pending");
		
		text=driver.findElement(By.xpath(Pending));
		return text.getText();
		
	    
	
	}


}
	
