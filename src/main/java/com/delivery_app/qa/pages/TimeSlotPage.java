package com.delivery_app.qa.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.delivery_app.qa.base.TestBase;
import com.delivery_app.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Step;

public class TimeSlotPage extends FunctionClass {
	public static WebDriver driver;
	String variablename;
	ArrayList<String> data;
	String TimeSlotIcon;
	
	

	// Initializing the page objects
		public TimeSlotPage() {

			PageFactory.initElements(driver, this);

		}

	
	
	public void TimeSlot() throws InterruptedException {
		try {
			setORprop();
			TimeSlotIcon = prop_OR.getProperty("TimeSlotIcon");	
		
			// finding list of timeslot filed
			List<WebElement> elements = driver.findElements(By.xpath("//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout"));
			
			System.out.println("list count:" + elements.size());
			for (int i = 0; i < elements.size(); i++) {
				
				
				// click on timeslot text by one by one
				elements.get(i).click();
				Thread.sleep(2000);
		
				System.out.println("Index of i is :-" + Integer.toString(i));
				
				clickElement(TimeSlotIcon);
				Thread.sleep(2000);
				
				String titlexpath="//android.widget.TextView[@resource-id='deliveryapp.countrydelight.in.deliveryapp:id/tv_time_slot']";
				
				
				WebElement nameText = driver.findElement(By.xpath(titlexpath));
				// printing title with description of FAQ
				System.out.println("Timeslot Selected Text : " + nameText.getText().toString());
		//		test.log(LogStatus.INFO, "Time Slot selection Successful");
			}
		}
			catch (Exception e) {
		//		test.log(LogStatus.FAIL, "Time Slot selection Failed"); 		
		
			}
		
	}
	
	@Step(" click on time slot button")
	public void clickonTimeSlotIcon() throws InterruptedException {
		
		try {
		setORprop();
		TimeSlotIcon = prop_OR.getProperty("TimeSlotIcon");
		waitUntilFound(TimeSlotIcon, 10);
		clickElement(TimeSlotIcon);

		}  catch (Exception e) {
			
		}
	
	}

}